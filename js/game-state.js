define(['game-db'], function(GameDB) {

	var gameState = {};
    
    gameState.init = function() {
        // reset gameState
        _.each(gameState, function(value, key) {
			if(typeof(value) != "function") {
				delete gameState[key];
			}
		});
        // Reset gameDb from custom values
        _.each(GameDB.regions, function(region, regionName) {
			_.each(region.resources, function(resourceName) {
				delete region.owned;
			});
        });
        
        gameState.playerName = "New player";
        gameState.money = GameDB.START_MONEY;
        gameState.date = moment();
            
        gameState.factories = {};
        gameState.currentFactory = {};
        gameState.lastBenefits = 0;
        gameState.tax = 1;
        gameState.taxIncrease = .2;

        var lastSavedGames = $.jStorage.get("savedGames");
        if(lastSavedGames == undefined){
            gameState.gameName = "Game 1";
        } else {
            var existingGames = $.jStorage.get("savedGames") || [];
            var idGame = 1;
            while (existingGames.indexOf("Game " + idGame) != -1) {
                idGame++;
            }
            gameState.gameName = "Game " + idGame;
        }
    }
    
	gameState.save = function(otherName) {
		var data = {};
		_.each(gameState, function(value, key){
			if(typeof(value) != "function"){
				data[key] = value;
			}
		});
        var gameName = otherName || gameState.gameName;
		$.jStorage.set(gameName, JSON.stringify(data));
		
		var savedGames = $.jStorage.get("savedGames");
		if(savedGames == undefined){
			savedGames = [];
		}
		savedGames = _.without(savedGames, gameName) 
		savedGames.unshift(gameName);
		$.jStorage.set("savedGames",savedGames);
	}
    
    gameState.removeSavedGame = function(gameName){
    	var savedGames = $.jStorage.get("savedGames");
		savedGames = _.without(savedGames, gameName) 
		$.jStorage.set("savedGames",savedGames);
        $.jStorage.deleteKey(gameName);
    }
	
	gameState.findExistingGames = function(gameName){
		var savedGames = $.jStorage.get("savedGames");
		if(savedGames == undefined){
			savedGames = [];
		}
		return savedGames;
	}
	
	gameState.load = function(gameName){
		var data = JSON.parse($.jStorage.get(gameName));
		_.each(data, function(value, key){
			gameState[key] = value;
		});
		gameState.date = moment(gameState.date);
	}
	
	gameState.createFactory = function(resourceName, regionName) {
		var factory = {};
		factory.resourceName = resourceName;
		factory.regionName = regionName;
		factory.buildings = [];
		factory.additionalWorkers = [];
		factory.benefits = 0;
		
		gameState.factories[regionName] = gameState.factories[regionName] || {};
		// TODO si resourceName == null, resourceName = factory1 ou factory2 ou factory3...
		gameState.factories[regionName][resourceName] = factory;
		return factory;
	}
	
	gameState.saveFactory = function(factory) {
		gameState.factories[factory.regionName] = gameState.factories[factory.regionName] || {};
		gameState.factories[factory.regionName][factory.resourceName] = factory;
        gameState.nextTurn();
	}
	
	gameState.findAllPlayerFactories = function(){
		var results = [];
		_.each(gameState.factories,function(region) {
			_.each(region,function(factoryName) {
				results.push(factoryName);
			});
		});
		return results;
	}
	
	gameState.findRegionPlayerFactories = function(region){
		return gameState.factories[region];
	}
	
	gameState.getFactory = function(region, factoryId) {
		if(gameState.factories[region] != undefined){
			return gameState.factories[region][factoryId];
		} else {
			return null;
		}
	}
	
	gameState.nextTurn = function() {
        gameState.date = gameState.date.add(1, 'month');
		var monthlyBenefits = this.getMonthlyBenefits();
        gameState.money += monthlyBenefits;
        gameState.lastBenefits = monthlyBenefits;
		gameState.tax += gameState.taxIncrease;
		gameState.showInternReport = true;
        
        // Auto-save
        gameState.save("Auto-save");
	}	
	
	gameState.getMonthlyBenefits = function() {
        var benefits = 0;
        _.each(gameState.findAllPlayerFactories(), function(factory) {
            benefits += factory.benefits;
        });
        return benefits - Math.floor(gameState.tax);
	}
	
	return gameState;
	
});
