define([], function() {

	return {
	
		CANVAS_ID: 'phaser',
	
		DEBUG: {
			GOTO_WORLDMAP: false,
			GOTO_FACTORY: false,
			DISABLE_TUTORIAL: false
		},
	
		TILE_SIZE: 60,		
		MIN_WIDTH: 480,
		MIN_HEIGHT: 300,
		MAX_WIDTH: 960,
		MAX_HEIGHT: 600
	};

});