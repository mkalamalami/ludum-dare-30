define(['consts', 'gui'], function(Consts, Gui) {

return {

	createPreview: function(state, tileGrid, zones) {
		return new FactoryPreview(state, tileGrid, zones);
	},
	
	isInRange: isInRange

}

function FactoryPreview(state, tileGrid, zones) {

	var previewTiles = [];
	var rangeTiles = [];
	var currentPreview = null;
	var currentRange = {tile:null, type:null};
	var terrain = createTerrainFromZones(zones);

	for (var i = 0; i < tileGrid.cols; i++) {
		var prevCol = [];
		var rangeCol = [];
		for (var j = 0; j < tileGrid.lines; j++) {
			var previewTile = state.add.sprite(
				tileGrid.x + i * Consts.TILE_SIZE,
				tileGrid.y + j * Consts.TILE_SIZE,
				'tile-preview');
			previewTile.visible = false;
			prevCol.push(previewTile);
			
			var rangeTile = state.add.sprite(
				tileGrid.x + i * Consts.TILE_SIZE,
				tileGrid.y + j * Consts.TILE_SIZE,
				'tile-range');
			rangeTile.visible = false;
			rangeCol.push(rangeTile);
		}
		previewTiles.push(prevCol);
		rangeTiles.push(rangeCol);
	}

	this.disablePreview = function() {
		if (currentPreview != null) {
			currentPreview = null;
			for (var i = 0; i < previewTiles.length; i++) {
				for (var j = 0; j < previewTiles[0].length; j++) {
					previewTiles[i][j].visible = false;
				}
			}
		}
	};
	
	this.disableRange = function() {
		if (currentRange.tile != null || currentRange.type != null) {
			currentRange = {tile:null, type:null};
			for (var i = 0; i < rangeTiles.length; i++) {
				for (var j = 0; j < rangeTiles[0].length; j++) {
					rangeTiles[i][j].visible = false;
				}
			}
		}
	};

	this.enablePreview = function(buildingType) {
		if (buildingType != currentPreview) {
			currentPreview = buildingType;
			for (var i = 0; i < previewTiles.length; i++) {
				for (var j = 0; j < previewTiles[0].length; j++) {
					previewTiles[i][j].visible = this.isAllowed(buildingType, i, j);
				}
			}
		}
	};

	this.enableRange = function(targetTile, buildingType) {
		if (targetTile != currentRange.tile || buildingType != currentRange.type) {
			currentRange.tile = targetTile;
			currentRange.type = buildingType;
			var hasRangeConstraints = buildingType.constraints &&
				  (buildingType.constraints.range
				|| buildingType.constraints.rangeX !== undefined
				|| buildingType.constraints.rangeY !== undefined);
			for (var i = 0; i < rangeTiles.length; i++) {
				for (var j = 0; j < rangeTiles[0].length; j++) {
					rangeTiles[i][j].visible = hasRangeConstraints
						&& this.isInRange(targetTile, buildingType, i, j);
				}
			}
		}
	};
	
	this.isAllowed = function(buildingType, i, j) {
		var constr = buildingType.constraints || {};
		if (constr.zone && terrain[i][j] != constr.zone) {
			return false;
		}
		if (constr.col !== undefined) {
			if (constr.col >= 0 && constr.col != i) {
				return false;
			}
			if (constr.col < 0 && previewTiles.length + constr.col != i) {
				return false;
			}
		}
		return true;
	};
	
	this.isInRange = isInRange;
	
	function createTerrainFromZones(zones) {
		var terrain = [];
		
		zones.forEach(function(zone) {
			for (var i = zone.x; i < zone.x + zone.width; i++) {
				for (var j = zone.y; j < zone.y + zone.height; j++) {
					if (!terrain[i]) terrain[i] = [];
					terrain[i][j] = zone.type;
				}
			}
		});
		
		return terrain;
	
	};

	return this;
	
};

function isInRange(targetTile, buildingType, i, j) {
	var constr = buildingType.constraints || {};
	if (constr.range
			&& (Math.abs(targetTile.i - i) > constr.range
			|| Math.abs(targetTile.j - j) > constr.range)) {
		return false;
	}
	if (constr.rangeX !== undefined && Math.abs(targetTile.i - i) > constr.rangeX) {
		return false;
	}
	if (constr.rangeY !== undefined && Math.abs(targetTile.j - j) > constr.rangeY) {
		return false;
	}
	return true;
};

});