define([], function() {

	Phaser.Filter.Water = function(game) {
		Phaser.Filter.call(this, game);
		this.fragmentSrc = document.getElementById('water-shader').innerHTML.split('\n');
	};

	Phaser.Filter.Water.prototype = Object.create(Phaser.Filter.prototype);
	Phaser.Filter.Water.prototype.constructor = Phaser.Filter.Water;

});