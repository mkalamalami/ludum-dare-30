define(['game-state', 'factory-building-state', 'game-db', 'consts'], 
		function(GameState, FactoryBuildingState, GameDB, Consts) {

	return {
		createFactoryRunner: function(state) {
			return new FactoryRunner(state);
		}
	};
	
	function FactoryRunner(state) {
	
		this.dayDuration; // in turns
		this.buildingStates;
		this.t;
		this.currentDay;
		
		this.start = function(dayDuration) {
			if (!this.isRunning()) {
				this.currentDay = 0;
				this.t = 0;
				this.dayDuration = dayDuration;
				this.buildingStates = initBuildingStates(state.data.tiles);
			}
			else {
				this.dayDuration = dayDuration;
			}
		};
	
		this.stop = function() {
			this.buildingStates.forEach(function(buildingState) {
				buildingState.destroyHeldSprites();
				buildingState.workers.forEach(function(worker) {
					var fadeOut = state.add.tween(worker.group); 
					fadeOut.to({alpha: 0., y: worker.group.y - Consts.TILE_SIZE}, 300,
						Phaser.Easing.Quadratic.InOut, true);
					fadeOut.onComplete.add(function() {
						worker.group.destroy(); // YEAAAAAAAAAAAAAAH TAKE THIS
					});
				});
			});
			this.buildingStates = null;
			this.currentDay = 1;
			state.data.texts.date.refreshText();
		};
		
		this.isRunning = function() {
			return this.buildingStates != null;
		};
		
		this.isFinished = function() {
			return this.currentDay == 31;
		};
		
		this.update = function() {
			if (this.isRunning() && !this.isFinished()) {
				if (this.t % this.dayDuration == 0) {
					this.buildingStates.forEach(function(buildingState) {
						buildingState.update(this.buildingStates, this.currentDay, this.dayDuration * 15);
					}, this);
					this.buildingStates.forEach(function(buildingState) {
						buildingState.updateWorkers(this.buildingStates, this.currentDay, this.dayDuration * 15);
					}, this);
					this.buildingStates.forEach(function(buildingState) {
						buildingState.updateSprites(this.buildingStates, this.currentDay, this.dayDuration * 15);
					}, this);
					this.currentDay++;
					state.data.texts.date.refreshText();
				}
				this.t++;
			}
		};
		
		function initBuildingStates(tiles) {
			var buildingStates = [];
			var buildingId = 1;
			tiles.forEach(function(col, i){
				col.forEach(function(tileBuilding, j) {
					if (tileBuilding) {
						buildingStates.push(FactoryBuildingState.createBuildingState(
							state, tileBuilding, {i:i, j:j}, buildingId++));
						if (tileBuilding.gui) {
							tileBuilding.gui.hide();
						}
					}
				});
			});
			return buildingStates;
		}
		
		function initWorkerStates() {
		
		}
		
		return this;
	}

});
