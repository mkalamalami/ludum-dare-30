define(['consts', 'lib/phaser'], function(Consts) {

return {
	createButton: createButton,
	notifySpentMoney: notifySpentMoney,
	notifyForbidden: notifyForbidden,
	createPopup: createPopup,
	createMuteButton: createMuteButton,
	runEndingAnim: runEndingAnim
};

function createMuteButton(state, musicName, loop) {
	var muteButton = state.add.button(state.world.width - 65, state.world.height - 65, 'soundIcon', function() {
		$.jStorage.set("mute", !$.jStorage.get("mute"));
		updateMuteFrame();
	}, this);
	muteButton.input.useHandCursor = true;
	updateMuteFrame();
	return muteButton;
	
	function updateMuteFrame() {
		state.sound.stopAll();
		if ($.jStorage.get("mute")) {
			muteButton.frame = 1;
		} else {
			state.add.sound(musicName).play('', 0, 1, loop);
			muteButton.frame = 0;
		}
	}
}

function runEndingAnim(state, gameState, x, y) {
	var rocket = state.add.sprite(x, y, 'ending');
	rocket.angle = 45;
	rocket.scale.set(0);
	var t1a = state.add.tween(rocket);
	t1a.to({y: y - 100}, 2000, Phaser.Easing.Quadratic.InOut, true);
	var t1b = state.add.tween(rocket.scale);
	t1b.to({x: .5, y: .5}, 2000, Phaser.Easing.Quadratic.InOut, true);
	t1b.onComplete.add(function() {
		var t2a = state.add.tween(rocket.scale);
		t2a.to({x: 1, y: 1}, 3000, Phaser.Easing.Quadratic.InOut, true);
		var t2b = state.add.tween(rocket);
		t2b.to({x: x + 10, y: y - 200, angle: 0}, 3000, Phaser.Easing.Quadratic.InOut, true);
		t2b.onComplete.add(function() {
			setTimeout(function() {
				rocket.frame = 1;
				setTimeout(function() {
					var t3 = state.add.tween(rocket);
					t3.to({x:-500, y: -500}, 1500, Phaser.Easing.Quadratic.InOut, true);
					t3.onComplete.add(function() {
						if (!gameState.notFirstEnding) {
									
							var popupGroup = state.add.group();
							var popup = state.add.sprite(state.world.centerX, state.world.centerY, 'gui-popup');
							popup.anchor.set(.5);
							popup.inputEnabled = true;
							popup.input.useHandCursor = true;
							popup.events.onInputDown.add(function() {
								var fadeOut = state.add.tween(popupGroup);
								fadeOut.to({alpha: 0., y: 0}, 1000, Phaser.Easing.Quadratic.InOut, true);
								fadeOut.onComplete.add(function() {
									popupGroup.destroy();
								});
							});
							popupGroup.add(popup);
							
							var endingText = state.add.text(280, 110,
								  'from: the.boss@corponnected.world\n'
								+ 'topic: Is it a bird? Is it a plane?\n\n'
								+ 'Dear collaborator,\n\n'
								+ 'I have a golden rule not to send work mails\n'
								+ 'during holidays, but I\'ll break it here...\n'
								+ '\'Cause I really want to say I am quite\nimpressed with your latest achievement!\n'
								+ 'A rocket to space?! Awesome. But with an\noctopus on top?! That\'s the most\nawesome thing I\'ve ever seen!\n\n'
								+ 'After that, I guess not muchh left for you\nto discover around here...'
								+ 'But hey, \nfeel free to continue to manage the company\nif you will... Or restart and beat your score:\n\n'
								+ '>>>> OCTOPUS SENT TO SPACE IN ' + gameState.date.format('MMMM YYYY') + '\n\n'
								+ 'Regards,\nThe Boss',
								{font: '11pt Courier', fill: 'white'});
							popupGroup.add(endingText);
							
							popupGroup.alpha = 0.
							var fadeIn = state.add.tween(popupGroup);
							fadeIn.to({alpha: 1., y: -30}, 1000, Phaser.Easing.Quadratic.InOut, true);
							
							gameState.notFirstEnding = true;
						}
					});
				}, 1000);
			}, 1000);
		});
	});
	
}

function notifySpentMoney(state, x, y, amount, options) {
	// available options:
	// size=20
	// fast=true
	options = options || {};
	options.size = options.size || 20;
	
	if (typeof amount == 'number' && amount >= 0) {
		amount= '+' + amount;
	}

	var money = state.add.text(x, y, amount + 'c', 
		{font: 'bold ' + options.size + 'pt Arial', fill: 'yellow'});
	money.anchor.set(0.5);
	var zoomIn = state.add.tween(money.scale);
	zoomIn.to({x: 1.3, y: 1.3}, (options.fast?100:1000), Phaser.Easing.Sinusoidal.None, true);
	zoomIn.onComplete.add(function() {
		var fadeOut = state.add.tween(money);
		fadeOut.to({y: money.y + money.height * 2, alpha: 0}, 800, Phaser.Easing.Quadratic.InOut, true);
		fadeOut.onComplete.add(function() { money.destroy(); });
	});
}

function notifyForbidden(state, x, y) {
	var no = state.add.sprite(x, y, 'no');
	no.anchor.set(0.5);
	var zoomIn = state.add.tween(no.scale);
	zoomIn.to({x: 1.3, y: 1.3}, 1000, Phaser.Easing.Sinusoidal.None, true);
	zoomIn.onComplete.add(function() {
		var fadeOut = state.add.tween(no);
		fadeOut.to({alpha: 0}, 800, Phaser.Easing.Quadratic.InOut, true);
		fadeOut.onComplete.add(function() { no.destroy(); });
	});
}

// Main menu button
function createButton(state, x, y, text, callback, options) {
	// available options:
	// centered=true
	// size=true
	// sprite=...
	options = options || {};

	var button = state.add.group();
	
	button.background = state.add.sprite(0, 0, options.sprite || 'gui-long');
	if (options.centered) {
		button.background.anchor.set(0.5);
	}
	button.background.inputEnabled = true;
	button.background.input.useHandCursor = true;
	button.background.events.onInputDown.add(callback, state);
	button.add(button.background);
	
	button.label = state.add.text(
		(options.centered) ? 0 : background.width / 2,
		(options.centered) ? 0 : background.height / 2,
		text,
		{font: 'bold ' + (options.size || 20) + 'pt Arial', fill: 'white'});
	button.label.align = 'center';
	button.label.anchor.set(0.5);
	button.add(button.label);
	
	button.x = x;
	button.y = y;
	
	return button;
};

function createPopup(state, group, hide) {
	var sprite = state.add.sprite(state.world.centerX, state.world.centerY, "gui-popup");
	sprite.anchor.setTo(0.5, 0.5);
	sprite.inputEnabled = true;
	state.game.canvas.style.cursor = "default";
	var xMin = state.world.centerX - 450/2;
	var xMax = state.world.centerX + 450/2;
	var yMin = state.world.centerY - 450/2;
	var yMax = state.world.centerY + 450/2;
	sprite.events.onInputDown.add(function(event){
		// Calculate the corners of the popup
        var mouse = event.game.input.mousePointer;
		if(mouse.x < xMin || mouse.x > xMax || mouse.y < yMin || mouse.y > yMax ){
            if(!hide){
                group.destroy();
                sprite.input.useHandCursor = false;
            } else {
                group.visible = false;
            }
		}
	},this);
	group.add(sprite);
	sprite.customPosition = {
		x: xMin,
		y: yMin,
		width : 450,
		height: 450
	}
	return sprite;
}


});
