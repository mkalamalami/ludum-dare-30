/**
 * 
 * Full version of the DB, with all the crazy resources we didn't have to time to implement.
 * Unused in the code, but kept here to get back some stuff if we have the time.
 * 
 **/


define(['factory-ai'], function(AI) {

	var GameDB = {
		START_MONEY : 100,
        SALARY : 8,
		zones: [
			"grass",
			"metal",
			"rock",
			"sand",
			"water"
		],
		fields : {
			"plains" : {
				label : "Plains",
				background : 'factory-plains.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones : [
					{type: 'metal', x: 8, y: 0, width: 5, height: 5},
					{type: 'grass', x: 0, y: 0, width: 8, height: 5}
				],
				cost : 10
			},
			"mountains" : {
				label : "Mountains",
				background : 'factory-mountains.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones :[
					{type: 'metal', x: 8, y: 0, width: 5, height: 5},
					{type: 'rock', x: 0, y: 0, width: 8, height: 5}
				],
				cost : 20
			},
			"deserts" : {
				label : "Deserts",
				background : 'factory-deserts.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones :[
					{type: 'metal', x: 8, y: 0, width: 5, height: 5},
					{type: 'sand', x: 0, y: 0, width: 8, height: 5}
				],
				cost : 20
			},
			"coasts" : {
				label : "Coasts",
				background : 'factory-coasts.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones :[
					{type: 'metal', x: 8, y: 0, width: 5, height: 5},
					{type: 'grass', x: 0, y: 0, width: 8, height: 5},
					{type: 'water', x: 0, y: 0, width: 3, height: 5}
				],
				cost : 50
			},
			"factory" : {
				label : "Factory",
				background : 'factory-basic.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones :[
					{type: 'metal', x: 0, y: 0, width: 13, height: 5}
				],
				cost : 50
			},
			"rocketfactory" : {
				label : "Rocket Factory",
				background : 'factory-rocket.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones :[
					{type: 'metal', x: 0, y: 0, width: 13, height: 5}
				],
				cost : 2000
			}
		},
		resources : {
			"apple" : {
				label : "Apple",
				field : "plains",
				produceBuilding : "appleTree",
				sellingPrice : 1
			},
			"apple-pallet" : {
                label : "Apple pallet",
				sellingPrice : 15
			},
			"iron" : {
				label : "Iron",
				field : "mountains",
				produceBuilding : "ironVein",
                sellingPrice : 20
			},
            "ironMiningCart" : {
				label : "Iron mining cart"
			},
			"rocket" : {
				label : "rocket",
				field : "rocketfactory",
				produceBuilding : "rocketBuilder",
				sellingPrice   : 5000
			},
			"cider" : {
				label : "Cider",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"apple_pie" : {
				label : "Apple Pie",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"dough" : {
				label : "Dough",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"pizza" : {
				label : "Pizza",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"wheat" : {
				label : "Wheat",
				field : "plains",
				produceBuilding : "wheatFields",
				sellingPrice   : 1
			},
			"hamburger" : {
				label : "Hamburger",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"cow" : {
				label : "Cow",
				field : "plains",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"cheese" : {
				label : "Emmental",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"weapon" : {
				label : "Weapon",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"coal" : {
				label : "Coal",
				field : "mountains",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"potato" : {
				label : "Potato",
				field : "plains",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"fuel" : {
				label : "Fuel",
				sellingPrice   : 20
			},
			"octopus" : {
				label : "Octopus",
				field : "coasts",
				produceBuilding : "octopusFishingBoat"
			},
            "octopus-pallet" : {
                label : "Octopus pallet",
				sellingPrice : 30
			},
			"books" : {
				label : "Books",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"barbecue" : {
				label : "Barbecue",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"flour" : {
				label : "Flour",
				sellingPrice   : 10
			},
			"steel" : {
				label : "Steel",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"crude_oil" : {
				label : "Crude Oil",
				field : "deserts",
				produceBuilding : "forage"
			},
			"bread" : {
				label : "Bread",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"wood" : {
				label : "Wood",
				field : "plains",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"potato_cannon" : {
				label : "Potato Cannon",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			},
			"potato_salad" : {
				label : "Potato Salad",
				field : "factory",
				produceBuilding : "",
				sellingPrice   : 500
			}
		},
		regions: {
			"France" : {
				resources: ["barbecue", "cider", "rocket"],
				simpleFactories: 1,
				x : 480,
				y: 175
			},
			"Canada" : {
				resources: ["octopus", "wood"],
				simpleFactories: 1,
				x : 185,
				y: 155
			},
			"Sweden" : {
				resources: ["dough", "wood"],
				simpleFactories: 1,
				x : 523,
				y: 130
			},
			"Italia" : {
				resources: ["pizza", "apple"],
				simpleFactories: 1,
				x : 522,
				y: 195
			},
			"China" : {
				resources: ["iron", "apple", "cow"],
				simpleFactories: 1,
				x : 775,
				y: 222
			},
			"Switzerland" : {
				resources: ["cheese"],
				simpleFactories: 1,
				x : 494,
				y: 181
			},
			"United States" : {
				resources: ["hamburger", "weapon", "coal", "rocket"],
				simpleFactories: 1,
				x : 220,
				y: 200
			},
			"Ohio" : {
				resources: ["potato_salad", "barbecue"],
				simpleFactories: 1,
				x : 258,
				y: 192
			},
			"India" : {
				resources: ["wheat", "coal"],
				simpleFactories: 1,
				x : 685,
				y: 242
			},
			"Russia" : {
				resources: ["potato", "weapon"],
				simpleFactories: 1,
				x : 617,
				y: 141
			},
			"Australia" : {
				resources: ["cider", "potato_cannon"],
				simpleFactories: 1,
				x : 832,
				y: 372
			},
			"South Africa" : {
				resources: ["iron", "coal"],
				simpleFactories: 1,
				x : 541,
				y: 385
			},
			"Brazil" : {
				resources: ["wood", "cow"],
				simpleFactories: 1,
				x : 345,
				y: 322
			},
			"Saudi Arabia" : {
				resources: ["crude_oil", "books"],
				simpleFactories: 1,
				x : 597,
				y: 236
			},
			"Japan" : {
				resources: ["octopus", "rocket"],
				simpleFactories: 1,
				x : 850,
				y: 204
			}
		},
		buildingTypes: {
			appleTree: {
				label: "Apple tree",
				description: "Produces an Apple every 3 turns. Capacity limited to 1.",
				cost: 10,
				unlock: [
					'storage'
				],
				constraints: {
					zone: ['grass']
				},
				category: "primary",
				buildingTurn: AI.primaryResourceGenerator("apple", 3, 1)
			},
            ironVein: {
				label: "Iron vein",
				description: "Produces an Iron pebble every 2 turns. Capacity limited to 2.", 
				cost: 10,
				unlock: [
					'ironMine', 'breaker', 'storage', 'pebbleTruck'
				],
				constraints: {
					zone: ['rock']
				},
                category: "primary",
				buildingTurn: AI.primaryResourceGenerator("pebbleWithIron", 2, 2)
			},
			ironMine: {
				label: "Iron mine",
				description: "Puts 5 Iron pebbles into a Mining cart.", 
				cost: 10,
				constraints: {
					zone: ['rock'],
                    range: 1
				},
                category: "processing",
				buildingTurn: AI.resourceTransformerGenerator(["pebbleWithIron"], [5],["ironMiningCart"], [1]),
                workerTurn: AI.transportWorkerGenerator(["pebbleWithIron"],["primary"], 5)
			},
			breaker: {
				label: "Breaker",
				description: "Processes Mining carts full of Iron pebbles. Gives 2 Iron and 3 Pebbles each.", 
				cost: 10,
                constraints: {
					zone: ['rock'],
                    range: 4
				},
                category: "processing",
				buildingTurn: AI.resourceTransformerGenerator(["ironMiningCart"], [1],["iron","pebble"],[2,3]),
                workerTurn: AI.transportWorkerGenerator(["ironMiningCart"],["processing"], 5)
			},
            pebbleTruck: {
				label: "Pebble truck",
				description: "Processes Mining carts full of Iron pebbles. Gives 2 Iron and 3 Pebbles each.", 
				cost: 10,
                constraints: {
					col: -1
				},
                category: "export",
                buildingTurn: AI.nothing,
                workerTurn: AI.transportWorkerGenerator(["pebble"],["processing"], 50)
			},
            wheatFields: {
				label: "Wheat fields",
				description: "Processes Mining carts full of Iron pebbles. Gives 2 Iron and 3 Pebbles each.", 
				cost: 5,
				unlock: [
					'mill','storage'
				],
				constraints: {
					zone: ['grass']
				},
                category: "primary",
				buildingTurn: AI.primaryResourceGenerator("wheat", 5, 1)
			},
            mill: {
				label: "Mill",
				cost: 20,
                constraints: {
					zone: ['grass'],
                    range: 2
				},
                category: "processing",
                buildingTurn: AI.resourceTransformerGenerator(["wheat"], [5],["flour"], [2]),
                workerTurn: AI.transportWorkerGenerator(["wheat"],["primary"], 10)
            },
            forage: {
                label: "Forage",
                cost: 100,
                unlock: [
					'barrelFillingMachine','storage'
				],
				constraints: {
					zone: ['sand']
				},
                category: "primary",
                buildingTurn: AI.primaryResourceGenerator("crude_oil", 1, 2)
            },
            barrelFillingMachine: {
                label: "Filling machine",
                cost: 50,
                constraints: {
					range: 1
				},
                category: "processing",
				buildingTurn: AI.resourceTransformerGenerator(["crude_oil"], [5],["fuel"], [1]),
                workerTurn: AI.transportWorkerGenerator(["crude_oil"],["primary"], 5)
            },
            rocketBuilder:{
                label: "Rocket Builder",
                cost: 500,
                unlock: [
					'importTruck_iron','importTruck_fuel','importStorage','launchingArea'
				],
                constraints: {
					rangeX: 1
				}, 
                category: "processing",
				buildingTurn: AI.resourceTransformerGenerator(["iron","fuel"], [10,10],["rocket"], [1]),
                workerTurn: AI.transportWorkerGenerator(["iron","fuel"],["storage"], 30)
            },
            octopusFishingBoat:{
                label:"Fishing boat",
                cost: 50,
                unlock: [
					'octopusDock'
				],
                constraints: {
                    zone: ['water']
                },
                category: "primary",
                buildingTurn: AI.primaryResourceGenerator("octopus", 5, 2)
            },
            octopusDock:{
                label:"Dock",
                cost: 100,
                constraints: {
                    col:3
                },
                category: "storage",
                buildingTurn: AI.resourceTransformerGenerator(["octopus"], [2],["octopus-pallet"], [1]),
                workerTurn: AI.transportWorkerGenerator(["octopus"],["primary"], 30)
            },
            importStorage: {
				label: "Storage",
				cost: 10,
				category: "storage",
				buildingTurn: AI.nothing,
				workerTurn: AI.transportWorkerGenerator(["iron","fuel"],["primary"], 10)
			},
			storage: {
				label: "Storage",
				cost: 10,
				category: "storage",
				buildingTurn: AI.storageBuildingTurn,
				workerTurn: AI.transportWorkerGenerator(["apple", "flour","iron","fuel"],["primary","processing"], 5)
			},
			importTruck_iron: {
				label: "Import Iron",
				cost: 10,
                category: "primary",
				constraints: {
					col: 0
				},
                buildingTurn: AI.primaryResourceGenerator("iron", 1, 1)
			},
            importTruck_fuel: {
				label: "Import Fuel",
                category: "primary",
				cost: 10,
				constraints: {
					col: 0
				},
                buildingTurn: AI.primaryResourceGenerator("fuel", 1, 1)
			},
			exportTruck: {
				label: "Export",
                category: "export",
				cost: 10,
				constraints: {
					col: -1
				},
				buildingTurn: AI.exportBuildingTurn,
				workerTurn: AI.exportWorkerGenerator(["apple-pallet", "flour","iron","fuel","octopus-pallet"])
			},
			launchingArea: {
				label: "Launching Area",
                category: "export",
				cost: 500,
				constraints: {
					col: -1
				},
				buildingTurn: AI.exportBuildingTurn,
				workerTurn: AI.exportWorkerGenerator(["rocket"],["processing"])
			}
		}
	};
	
	// Building names
	_.each(GameDB.buildingTypes, function(buildingType, key) {
		buildingType.name = key;
	});
	
	return GameDB;
});
