define(['gui', 'factory-preview'], function(Gui, FactoryPreview) { // Can't import GameDB & GameState here. Use this.db, this.gameState

	var IA = {};
	
	IA.primaryResourceGenerator = function(resourceName, nTurns, maxCapacity, maxProduced, nProduced) {
		return function(buildingStates, currentDay) {
            nProduced = nProduced || 1;
            for(var n = 0; n < nProduced; n++){
                generateEveryNTurns(this, resourceName, nTurns, currentDay, maxCapacity, maxProduced);
            }
		};
	};
	IA.resourceTransformerGenerator = function(resources, needs, resourcesCreated, nCreated) {
		return function processResource(buildingStates, currentDay) {
            var dropSuccess = true;
            var drops = [];
            var building = this;
            _.each(resources,function(resource, i){
                dropSuccess = dropSuccess && building.drop(resource, needs[i]);
                if(dropSuccess){
                    drops.push(resource);
                } else {
                    return;
                }
            });
            if(dropSuccess){
                _.each(resourcesCreated,function(resource, i){
                    building.hold(resource, nCreated[i]);
                });
            } else {
                _.each(drops,function(resource, i){
                    building.hold(resource, needs[resources.indexOf(resource)]);
                });
            }
        };
	};
	IA.transportWorkerGenerator = function(resources, buildingTypes, storageSize) {
		return function(buildingStates, currentDay) {
            if (this.busy) {
				this.resolveBusy();
            }
            if (this.held) {
                storeResource(this, storageSize);
            }
            else {
                fetchResource(this, buildingStates, resources, buildingTypes);
            }
        };
	};	
	
	IA.exportWorkerGenerator = function(resources, buildingTypes) {
		return function(buildingStates, currentDay) {
            if (this.busy) {
				this.resolveBusy();
            }
            if (this.held) {
                storeResource(this);
            }
            else {
                buildingTypes = buildingTypes || ["storage"];
                fetchResource(this, buildingStates, resources, buildingTypes);
            }
        };
	};
    
    IA.storageBuildingTurn = function(buildingStates, currentDay) {
		if (this.drop("apple", 5)) {
			this.hold("apple-pallet");
		}
		if (this.held["wood"]) {
			this.arrivalDates = this.arrivalDates || [];
			this.previousAmount = this.previousAmount || 0;
			for (var i = this.previousAmount; i < this.held["wood"]; i++) {
				this.arrivalDates.push(currentDay);
			}
			var readyCount = _.countBy(this.arrivalDates, function(date) { return date == currentDay - 5; }).true;
			if (readyCount > 0 && this.drop("wood", readyCount)) {
				this.hold("drywood", readyCount);
			}
			this.previousAmount = this.held["wood"];
		}
	};
	
	IA.exportBuildingTurn = function(buildingStates, currentDay) {
		_.each(this.held, function(amount, name) {
			var gains = this.db.resources[name].sellingPrice * amount;
			this.drop(name, amount);
			if (gains != 0) {
				makeMoney(this, gains);
			}
		}, this);
	};
    
    
    IA.rocketBuildingTurn = function(buildingStates, currentDay) {
        this.boardingPass = this.boardingPass || false;
		if(!this.boardingPass  && this.drop("rocket", 1)){
            this.boardingPass = true;
            this.hold("boardingPass", 1);
            this.hold("rocket", 1);
        }
        if(currentDay == 30 && this.drop("octopus", 1) && this.drop("rocket", 1)){
            var gains = this.db.resources["rocket"].sellingPrice;
			if (gains != 0) {
				makeMoney(this, gains);
			}
            Gui.runEndingAnim(this.state, this.gameState, this.building.x, this.building.y);
        }
	};
    
    IA.nothing = function(buildingStates, currentDay) {
    };
	
	
	
	// Building actions
	
	function generateEveryNTurns(buildingState, resource, n, currentDay, maxCapacity, maxProduced) {
		buildingState.held[resource] = buildingState.held[resource] || 0;
		buildingState.amountProduced = buildingState.amountProduced || 0;
		if (buildingState.held[resource] == 0 && !buildingState.startDay) {
			buildingState.startDay = currentDay;
		}
		if (((currentDay - buildingState.startDay) % n == 1 || n == 1)
				&& buildingState.held[resource] < (maxCapacity || 1)
				&& buildingState.amountProduced < (maxProduced || 30)) {
			buildingState.hold(resource);
			buildingState.amountProduced++;
		}
	};
	
	function spendMoney(self, amount, options) {
		return makeMoney(self, -amount);
	}
	
	function makeMoney(self, amount, options) {
		self.gameState.currentFactory.benefits += amount;
		self.state.data.texts.benefits.refreshText();
		Gui.notifySpentMoney(self.state, self.building.x, self.building.y, amount, options);
	}
	
	// Worker actions
	
	function fetchResource(worker, buildingStates, resources, buildingCategories) {
		var targetBuilding = null;
		for (i in resources) {
			var resourceName = resources[i];
			var targetBuildingState = findBuildingStateWith(
				worker, buildingStates, resourceName, buildingCategories);
			if (targetBuildingState) {
				targetBuildingState.booked[resourceName] = true;
				worker.busy = true;
				worker.goTo(targetBuildingState.building);
				worker.resolveBusy = function() {
					targetBuildingState.booked[resourceName] = false;
					if (targetBuildingState.drop(resourceName)) {
						worker.held = resourceName;
					}
					worker.busy = false;
				}
				return;
			};
		}
	};
	
	function storeResource(worker, maxStorageSize) {
		worker.busy = true;
		worker.goTo(worker.buildingState.building);
		worker.resolveBusy = function() {
			if (worker.buildingState.hold(worker.held, 1, maxStorageSize)) {
				worker.held = null;
				worker.busy = false;
			}
		};
	};
	
	// Utils
	
	function findBuildingStateWith(worker, buildingStates, resource, buildingCategories) {
		var foundState = null;
		for (i in buildingStates) {
			var buildingState = buildingStates[i];
			var resourceCount = 0;
			if (buildingState.held[resource] > resourceCount
				&& !buildingState.booked[resource]
				&& (!buildingCategories || buildingCategories.indexOf(buildingState.type.category) != -1)
				&& FactoryPreview.isInRange(worker.buildingState.tile,
					worker.db.buildingTypes[worker.buildingState.building.type], 
					buildingState.tile.i,
					buildingState.tile.j)) {
				foundState = buildingState;
				resourceCount = buildingState.held[resource];
			}
		}
		return foundState;
	};
	

	return IA;
});
