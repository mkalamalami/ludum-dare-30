define(['factory-ai'], function(AI) {

	var GameDB = {
		START_MONEY : 100,
        SALARY : 8,
		zones: [
			"grass",
			"metal",
			"rock",
			"sand",
			"water"
		],
		fields : {
			"plains" : {
				label : "Plains",
				background : 'factory-plains.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones : [
					{type: 'metal', x: 8, y: 0, width: 5, height: 5},
					{type: 'grass', x: 0, y: 0, width: 8, height: 5}
				],
				cost : 10
			},
			"mountains" : {
				label : "Mountains",
				background : 'factory-mountains.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones :[
					{type: 'metal', x: 8, y: 0, width: 5, height: 5},
					{type: 'rock', x: 0, y: 0, width: 8, height: 5}
				],
				cost : 500
			},
			"deserts" : {
				label : "Deserts",
				background : 'factory-deserts.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones :[
					{type: 'metal', x: 8, y: 0, width: 5, height: 5},
					{type: 'sand', x: 0, y: 0, width: 8, height: 5}
				],
				cost : 500
			},
			"coasts" : {
				label : "Coasts",
				background : 'factory-coasts.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones :[
					{type: 'metal', x: 8, y: 0, width: 5, height: 5},
					{type: 'grass', x: 0, y: 0, width: 8, height: 5},
					{type: 'water', x: 0, y: 0, width: 3, height: 5}
				],
				cost : 500
			},
			"factory" : {
				label : "Factory",
				background : 'factory-basic.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones :[
					{type: 'metal', x: 0, y: 0, width: 13, height: 5}
				],
				cost : 500
			},
			"rocketfactory" : {
				label : "Rocket Factory",
				background : 'factory-rocket.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones :[
					{type: 'metal', x: 0, y: 0, width: 13, height: 5}
				],
				cost : 2000
			},
			"potatoRoom" : {
				label : "Potato Room",
				background : 'factory-basic.jpg',
				grid: {
					x: 108,
					y: 160,
					cols: 13,
					lines: 5
				},
				zones :[
					{type: 'metal', x: 0, y: 0, width: 13, height: 5}
				],
				cost : 1000
			}
		},
		resources : {
			"apple" : {
				label : "Apple",
				field : "plains",
				produceBuilding : "appleTree",
				sellingPrice : 1
			},
			"apple-pallet" : {
                label : "Apple pallet",
				sellingPrice : 15
			},
			"iron" : {
				label : "Iron",
				field : "mountains",
				produceBuilding : "ironVein",
                sellingPrice : 25
			},
            "ironMiningCart" : {
				label : "Iron mining cart"
			},
			"rocket" : {
				label : "rocket",
				field : "rocketfactory",
				produceBuilding : "astronautsBus",
				sellingPrice   : 5000
			},
			"wheat" : {
				label : "Wheat",
				field : "plains",
				produceBuilding : "wheatFields",
				sellingPrice   : 1
			},
			"fuel" : {
				label : "Fuel",
				sellingPrice   : 30
			},
			"octopus" : {
				label : "Octopus",
				field : "coasts",
				produceBuilding : "octopusFishingBoat"
			},
            "octopus-pallet" : {
                label : "Octopus pallet",
				sellingPrice : 30
			},
			"flour" : {
				label : "Flour",
				sellingPrice   : 11
			},
			"crude_oil" : {
				label : "Crude Oil",
				field : "deserts",
				produceBuilding : "forage"
			},
			"wood" : {
				label : "Wood",
				field : "plains",
				produceBuilding : "tree"
			},
			"drywood" : {
				label : "Dry Wood",
				sellingPrice : 5
			},
			"potatosalad" : {
				label : "Potato Salad",
				field : "potatoRoom",
				produceBuilding : "potatosalad",
				sellingPrice   : 500
			},
            "boardingPass" : {
				label : "Boarding pass",
            },
		},
		regions: {
			"France" : {
				resources: ["wheat", "rocket"],
				simpleFactories: 1,
				x : 480,
				y: 175
			},
			"Canada" : {
				resources: ["iron", "wood"],
				simpleFactories: 1,
				x : 185,
				y: 155
			},
			"Sweden" : {
				resources: ["wood"],
				simpleFactories: 1,
				x : 523,
				y: 130
			},
			"Italia" : {
				resources: ["apple"],
				simpleFactories: 1,
				x : 522,
				y: 195
			},
			"China" : {
				resources: ["iron", "apple"],
				simpleFactories: 1,
				x : 775,
				y: 222
			},
			"United States" : {
				resources: ["apple", "rocket"],
				simpleFactories: 1,
				x : 220,
				y: 200
			},
			"Ohio" : {
				resources: ["potatosalad"],
				simpleFactories: 1,
				x : 258,
				y: 192
			},
			"India" : {
				resources: ["wheat"],
				simpleFactories: 1,
				x : 685,
				y: 242
			},
			"Russia" : {
				resources: ["iron", "rocket"],
				simpleFactories: 1,
				x : 617,
				y: 141
			},
			"Australia" : {
				resources: ["wood"],
				simpleFactories: 1,
				x : 832,
				y: 372
			},
			"South Africa" : {
				resources: ["iron", "octopus"],
				simpleFactories: 1,
				x : 541,
				y: 385
			},
			"Brazil" : {
				resources: ["wheat", "crude_oil"],
				simpleFactories: 1,
				x : 345,
				y: 322
			},
			"Saudi Arabia" : {
				resources: ["crude_oil"],
				simpleFactories: 1,
				x : 597,
				y: 236
			},
			"Japan" : {
				resources: ["octopus", "rocket"],
				simpleFactories: 1,
				x : 850,
				y: 204
			}
		},
		buildingTypes: {
			tree: {
				label: "Tree",
				description: "Produces Wood every 3 turns, up to 3 Wood.",
				cost: 11,
				unlock: [
					'storage'
				],
				constraints: {
					zone: ['grass']
				},
				category: "primary",
				buildingTurn: AI.primaryResourceGenerator("wood", 3, 10, 3)
			},
			appleTree: {
				label: "Apple tree",
				description: "Produces an Apple every 3 turns. Capacity limited to 1.",
				cost: 10,
				unlock: [
					'storage'
				],
				constraints: {
					zone: ['grass']
				},
				category: "primary",
				buildingTurn: AI.primaryResourceGenerator("apple", 6, 1)
			},
            ironVein: {
				label: "Iron vein",
				description: "Produces an Iron pebble every 2 turns. Capacity limited to 2.", 
				cost: 10,
				unlock: [
					'ironMine', 'breaker', 'storage', 'pebbleTruck'
				],
				constraints: {
					zone: ['rock']
				},
                category: "primary",
				buildingTurn: AI.primaryResourceGenerator("pebbleWithIron", 4, 2)
			},
			ironMine: {
				label: "Iron mine",
				description: "Puts 5 Iron pebbles into a Mining cart.", 
				cost: 100,
				constraints: {
					zone: ['rock'],
                    range: 1
				},
                category: "processing",
				buildingTurn: AI.resourceTransformerGenerator(["pebbleWithIron"], [8],["ironMiningCart"], [2]),
                workerTurn: AI.transportWorkerGenerator(["pebbleWithIron"],["primary"], 10)
			},
			breaker: {
				label: "Breaker",
				description: "Processes Mining carts full of Iron pebbles.\nGives 2 Iron and 3 Pebbles each.", 
				cost: 250,
                constraints: {
					zone: ['rock'],
                    range: 2,
                    rangeY: 0
                    
				},
                category: "processing",
				buildingTurn: AI.resourceTransformerGenerator(["ironMiningCart"], [1],["iron","pebble"],[2,3]),
                workerTurn: AI.transportWorkerGenerator(["ironMiningCart"],["processing"], 5)
			},
            pebbleTruck: {
				label: "Pebble truck",
				description: "Processes Mining carts full of Iron pebbles.\nGives 2 Iron and 3 Pebbles each.", 
				cost: 10,
                constraints: {
					col: -1,
                    rangeY : 0
				},
                category: "export",
                buildingTurn: AI.nothing,
                workerTurn: AI.transportWorkerGenerator(["pebble"],["processing"], 50)
			},
            wheatFields: {
				label: "Wheat fields",
				description: "Produces Wheat every 5 turns. Capacity limited to 1.", 
				cost: 5,
				unlock: [
					'mill','storage'
				],
				constraints: {
					zone: ['grass']
				},
                category: "primary",
				buildingTurn: AI.primaryResourceGenerator("wheat", 5, 1)
			},
            mill: {
				label: "Mill",
				description: "Processes 8 Wheat into 2 Bags of Flour.", 
				cost: 40,
                constraints: {
					zone: ['grass'],
                    range: 1
				},
                category: "processing",
                buildingTurn: AI.resourceTransformerGenerator(["wheat"], [8],["flour"], [2]),
                workerTurn: AI.transportWorkerGenerator(["wheat"],["primary"], 10)
            },
            forage: {
                label: "Oil rig", // Forage
				description: "Produces Crude oil every turn. Capacity limited to 2.", 
                cost: 90,
                unlock: [
					'barrelFillingMachine','storage'
				],
				constraints: {
					zone: ['sand']
				},
                category: "primary",
                buildingTurn: AI.primaryResourceGenerator("crude_oil", 2, 1)
            },
            barrelFillingMachine: {
                label: "Filling machine",
				description: "Processes 5 Crude oil units into 1 Fuel barrel.", 
                cost: 50,
                constraints: {
					range: 1
				},
                category: "processing",
				buildingTurn: AI.resourceTransformerGenerator(["crude_oil"], [5],["fuel"], [1]),
                workerTurn: AI.transportWorkerGenerator(["crude_oil"],["primary"], 10)
            },
            rocket_factory:{
                label: "Rocket Factory",
				description: "Processes 5 Iron and 5 Fuel barrels to build\nthe dream of every kid: a space rocket!", 
                cost: 500,
                constraints: {
					range: 1
				}, 
                category: "processing",
				buildingTurn: AI.resourceTransformerGenerator(["iron","fuel"], [5,5],["rocket"], [1]), // TODO Add octopus!
                workerTurn: AI.transportWorkerGenerator(["iron","fuel"],["storage"], 30)
            },
            octopusFishingBoat:{
                label:"Fishing boat",
				description: "Produces a... What? Two octopus? Okay. Two octopus,\nevery 7 turns. Capacity limited to 2.", 
                cost: 50,
                unlock: [
					'octopusDock'
				],
                constraints: {
                    zone: ['water']
                },
                category: "primary",
                buildingTurn: AI.primaryResourceGenerator("octopus", 7, 2, null, 2)
            },
            octopusDock:{
                label:"Dock",
				description: "Packages five Octopuses into a Pallet.\nDon't ask why, just build it.", 
                cost: 100,
                constraints: {
                    col:3,
                    rangeY:1
                },
                category: "storage",
                buildingTurn: AI.resourceTransformerGenerator(["octopus"], [5],["octopus-pallet"], [1]),
                workerTurn: AI.transportWorkerGenerator(["octopus"],["primary"], 30)
            },
            importStorage: {
				label: "Storage",
				description: "Stores imported goods (Iron or Fuel), up to 10 units.", 
				cost: 10,
                constraints: {
                    rangeY:1
				},
				category: "storage",
				buildingTurn: AI.nothing,
				workerTurn: AI.transportWorkerGenerator(["iron","fuel"],["primary"], 10)
			},
			storage: {
				label: "Storage",
				description: "Stores various goods (Apples, Wood, Flour, Iron, Fuel).\n5 Apples are packaged into an Pallet,\nWood turns to Dry wood after 5 days.", 
				cost: 10,
                constraints: {
                    rangeY:1
				},
				category: "storage",
				buildingTurn: AI.storageBuildingTurn,
				workerTurn: AI.transportWorkerGenerator(["apple", "flour","iron","fuel", "wood"],["primary","processing"], 5)
			},
			importTruck_iron: {
				label: "Import Iron",
				description: "Always available Iron.",// When grabbed by a worker,\ncosts N coins each.", // TODO
				cost: 10,
                category: "primary",
				constraints: {
					col: 0,
                    rangeY:1
				},
                buildingTurn: AI.primaryResourceGenerator("iron", 1,1, 5)
			},
            importTruck_fuel: {
				label: "Import Fuel",
				description: "Always available Fuel.",// When grabbed by a worker,\ncosts N coins each.", // TODO
                category: "primary",
				cost: 10,
				constraints: {
					col: 0,
                    rangeY:1
				},
                buildingTurn: AI.primaryResourceGenerator("fuel", 1, 1,5)
			},
			exportTruck: {
				label: "Export",
				description: "Exports finished goods: Pallets, Dry wood, Flour, Iron, Fuel.",
                category: "export",
				cost: 10,
				constraints: {
					col: -1,
                    rangeY:1
				},
				buildingTurn: AI.exportBuildingTurn,
				workerTurn: AI.exportWorkerGenerator(["apple-pallet", "flour","iron","fuel","octopus-pallet", "drywood"])
			},
			rocket_launcher: {
				label: "Rocket launcher",
				description: "Where your rocket will be launched toward new worlds!\nRequires 1 Rocket, and makes you win the game.",
                category: "export",
				cost: 500,
				constraints: {
					col: 6,
                    rangeY: 0
				},
				buildingTurn: AI.rocketBuildingTurn,
				workerTurn: AI.transportWorkerGenerator(["rocket","octopus"],["processing"])
			},
			potatosalad: {
				label: "Potato salad",
				description: "It's a potato salad. You can have a bite, I just have\nto figure out how to send it.",
                category: "primary",
				cost: 3
			},
			astronautsBus: {
				label: "Astronauts Bus",
				description: "It's the bus for the astronauts.\nWithout them, the rocket will not be launched.",
                category: "processing",
				cost: 3,
                constraints: {
					col: 0,
                    rangeY: 0
				},
                unlock: [
					'importTruck_iron','importTruck_fuel','importStorage','rocket_factory','rocket_launcher'
				],
                buildingTurn: AI.resourceTransformerGenerator(["boardingPass"], [1],["octopus"], [1]),
                workerTurn: AI.transportWorkerGenerator(["boardingPass"],["export"]),
			}
		}
	};
	
	// Building names
	_.each(GameDB.buildingTypes, function(buildingType, key) {
		buildingType.name = key;
	});
	
	return GameDB;
});
