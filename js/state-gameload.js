define(['gui', 'consts', 'game-state'], function(GuiUtils, Consts, GameState) {

	var state = new Phaser.State();
	var gui = {};

	state.create = function() {
		this.stage.backgroundColor = '#000000';
		
		gui.groupMenu = state.add.group();
        gui.groupMenu.visible = true;
		var games = GameState.findExistingGames();
        games = games.slice(0, 5);

		_.each(games,function(name, i){
			button = GuiUtils.createButton(state, state.world.centerX, i*80 + 100, name, function() {
				GameState.load(name);
				this.state.start('worldmap');
			}, {centered: true});
			gui.groupMenu.add(button);
            
            var deleteBtn = state.add.button(button.x + button.width/2 - 40, button.y , 'no', deleteSavedGame, state);
            deleteBtn.anchor.set(0.5);
            deleteBtn.gameName = name;
            gui.groupMenu.add(deleteBtn);
		});
		button = GuiUtils.createButton(state, state.world.centerX, games.length * 80 + 100, "Quit", function() {
			this.state.start('mainmenu');
		}, {centered: true});
		gui.groupMenu.add(button);
	};
	return state;

    function deleteSavedGame(button){
        GameState.removeSavedGame(button.gameName);
        this.state.start('gameload');
    }
});