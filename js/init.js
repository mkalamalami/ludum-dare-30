require(['lib/underscore', 'lib/phaser', 'lib/json2', 'lib/jstorage', 'lib/moment'], function() {

	require(['consts', 'assets','state-boot', 'state-preloader', 'state-mainmenu', 'state-worldmap', 'state-factory', 'state-gameload'],
		function(Consts, Assets, StateBoot, StatePreloader, StateMainMenu, StateWorldMap, StateFactory, StateGameLoad) {

		var game = new Phaser.Game(Consts.MAX_WIDTH, Consts.MAX_HEIGHT, Phaser.AUTO, 'phaser', {	
			create: function() {
				game.state.add('boot', StateBoot);
				game.state.add('preloader', StatePreloader);
				game.state.add('mainmenu', StateMainMenu);
				game.state.add('worldmap', StateWorldMap);
				game.state.add('factory', StateFactory);
				game.state.add('gameload', StateGameLoad);

				game.state.start('boot');
			}
			
		});

	});

});