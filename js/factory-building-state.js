define(['consts', 'game-db', 'game-state', 'factory-worker'],
	function(Consts, GameDB, GameState, FactoryWorker) {

return {

	createBuildingState: function(state, building, tile, buildingId) {
		return new BuildingState(state, building, tile, buildingId);
	}

}

function BuildingState(state, building, tile, buildingId) {

	var workerId = 1;

	this.id = buildingId;
	this.state = state;
	this.gameState = GameState; // ugly but needed to prevent circ. deps.
	this.db = GameDB;
	this.typeName = building.type;
	this.type = GameDB.buildingTypes[building.type];
	this.building = building;
	this.tile = tile;
	this.workers = (this.type.category == "primary")
		? [] : [FactoryWorker.createWorker(state, this, tile, buildingId*100 + workerId++)];
	this.held = {};
	this.heldSprites = [];
	this.heldTotal = 0;
	this.booked = {};
	
	_.each(GameState.currentFactory.additionalWorkers, function(workerTile) {
		if (workerTile.i == tile.i && workerTile.j == tile.j) {
			this.workers.push(FactoryWorker.createWorker(
				state, this, tile, buildingId*100 + workerId++));
		}
	}, this);
	
	this.destroyHeldSprites = function() {
		_.each(this.heldSprites, function(heldSprite) {
			heldSprite.destroy();
		});
	};
	
	this.hold = function(resourceName, amount, max) {
		amount = amount || 1;
		if (!this.held[resourceName]) {
			this.held[resourceName] = 0;
		}
		if (!max || this.heldTotal + amount <= max) {
			this.held[resourceName] += amount;
			return true;
		}
		else {
			return false;
		}
	};
	
	this.drop = function(resourceName, amount) {
		amount = amount || 1;
		if (!this.held[resourceName]) this.held[resourceName] = 0;
		if (this.held[resourceName] >= amount) {
			this.held[resourceName] -= amount;
			return true;
		}
		else {
			return false;
		}
	};
	
	this.updateWorkers = function(buildingStates, currentDay, delta) {
		this.workers.forEach(function(worker) {
			worker.update(buildingStates, currentDay, delta);
		});
	};
	
	this.update = function(buildingStates, currentDay, delta) {
		if (this.type.buildingTurn) {
			this.type.buildingTurn.apply(this, [buildingStates, currentDay]);
		}
		else if (!state.shownErrors["building" + building.type]) {
			state.shownErrors["building" + building.type] = true;
			//console.error("Warning: " + building.type + " buildings do nothing");
		}
	};
	
	this.updateSprites = function(buildingStates, currentDay, delta) {
		// Update held sprites
		var contentsChanged = false;
		var heldCounter = _.clone(this.held);
		_.each(this.heldSprites, function(heldSprite) {
			heldCounter[heldSprite] = heldCounter[heldSprite] - 1 || -1;
		});
		_.each(heldCounter, function(counter) {
			if (counter !== 0) contentsChanged = true;
		});
		
		if (contentsChanged) {
			// Destroy all
			_.each(this.heldSprites, function(heldSprite) {
				heldSprite.destroy();
			});
			this.heldSprites = [];
		
			// Create all
			this.heldTotal = 0;
			_.each(this.held, function(heldCount) {
				this.heldTotal += heldCount;
			}, this);
			var spaceBetweenSprites = Math.min(10, 45 / this.heldTotal);
			var n = 0;
			_.each(this.held, function(amount, heldName) {
				for (var i = 0; i < amount; i++) {
					var heldSprite = state.add.sprite(
						this.building.x - this.building.width / 2 + n++ * spaceBetweenSprites,
						this.building.y + 10, heldName);
					heldSprite.scale.set(.7);
					heldSprite.resourceName = this.held;
					this.heldSprites.push(heldSprite);
				}
			}, this);
		}
		
	}
	
	return this;
	
};


});
