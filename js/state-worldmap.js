define(['consts', 'game-state', 'game-db', 'gui', 'lib/mout/lang/deepClone', 'world-map-ui'],
		function(Consts, GameState, GameDB, GuiUtils, DeepClone) {

    var state = new Phaser.State();

    WorldMap = function(state) {
        this.state = state;
    }

    WorldMap.prototype.enterInFactory = function(factory) {
		GameState.currentFactory = DeepClone(factory);
        this.state.state.start('factory');
    }
	
	WorldMap.prototype.buyNewFactoy = function(factory) {
		GameState.money -= factory.cost;
		GameState.currentFactory = DeepClone(GameState.createFactory(factory.name, factory.regionName));
        this.state.state.start('factory');
    }

    WorldMap.prototype.nextTurn = function() {
		GameState.nextTurn();
    }

    var worldMap = new WorldMap(state);
    var worldMapUI = new WorldMapUI(state, worldMap); // will be populated at creation

    state.preload = function() {
	
		if (!GameState.firstWorldShow) {
			GameState.firstWorldShow = true;
		}
		else {
			state.sound.stopAll();
			this.music = this.add.audio('world');
			if (!$.jStorage.get("mute")) this.music.play('', 0, 1, true);
		}
		
	}
	
    state.create = function() {
        this.stage.backgroundColor = '#182d3b';

        worldMapUI.create();
    };

    state.update = function() {
        worldMapUI.waterFilter.update();
    };

    state.render = function() {
        // Input debug info
    //    state.game.debug.font = '8px Consolas';
     //   state.game.debug.inputInfo(10, state.world.height / 2);
    };

    return state;
	
});
