define(['consts', 'game-db', 'game-state', 'gui'], function(Consts, GameDB, GameState, Gui) {

return {

	createWorker: function(state, buildingState, tile, workerId) {
		return new Worker(state, buildingState, tile, workerId);
	}

}

function Worker(state, buildingState, tile, workerId) {

	this.id = workerId;
	this.state = state;
	this.gameState = GameState; // ugly but needed to prevent circ. deps.
	this.db = GameDB;
	this.buildingState = buildingState;
	this.tile = tile;
	
	this.takeTurn = GameDB.buildingTypes[buildingState.typeName].workerTurn;
	
	this.held = null; // The name of the held resource. A worker can only hold one
	this.heldSprite = null;
	
	this.group = state.add.group();
	this.group.x = state.data.tileGrid.x + (this.tile.i) * Consts.TILE_SIZE;
	this.group.y = state.data.tileGrid.y + (this.tile.j - 1) * Consts.TILE_SIZE;
	this.group.alpha = 0;
	var zoomIn = state.add.tween(this.group);
	zoomIn.to({alpha: 1., y: this.group.y + Consts.TILE_SIZE},
		300, Phaser.Easing.Quadratic.InOut, true);
	
	this.sprite = state.add.sprite(5 * (workerId-1)%100, 15, 'worker');
	this.group.add(this.sprite);
	
	this.idSprite = state.add.text(25 + 5 * (workerId-1)%100, 46, this.id,
		{font: '5pt Arial', fill: 'black'});
	this.idSprite.anchor.set(.5);
	this.group.add(this.idSprite);
	
	Gui.notifySpentMoney(state, this.sprite.x + this.group.x + 25, this.sprite.y + this.group.y + 110, -10,
		{size: 12, fast: true});
	
	this.leftHanded = Math.random() < .2;
	
	this.update = function(buildingStates, currentDay, delta) {
	
		this.goTo = function(building) {
			state.add.tween(this.group)
				.to({x: building.x - building.width/2, y: building.y - building.height/2},
					delta, Phaser.Easing.Quadratic.InOut, true);
		}
		
		if (this.takeTurn) {
			this.takeTurn(buildingStates, currentDay);
		}
		
		else if (!state.shownErrors["worker" + buildingState.typeName]) {
			state.shownErrors["worker" + buildingState.typeName] = true;
			console.error("Warning: " + buildingState.typeName + " workers do nothing");
		}
		
		// Update held sprite
		if (this.held && (!this.heldSprite || this.heldSprite.resourceName != this.held)) {
			if (this.heldSprite) this.heldSprite.destroy();
			this.heldSprite = state.add.sprite(this.leftHanded ? 40 : 13, 35, this.held);
			this.heldSprite.scale.set(.7);
			this.heldSprite.resourceName = this.held;
			this.group.add(this.heldSprite);
		}
		else if (!this.held && this.heldSprite) {
			this.heldSprite.destroy();
			this.heldSprite = null;
		}
	}
	
	return this;
}
	
});
