define(['consts', 'gui', 'factory-preview', 'factory-runner', 'game-db', 'game-state'],
	function(Consts, Gui, FactoryPreview, FactoryRunner, GameDB, GameState) {

var state = new Phaser.State();

state.preload = function() {

	state.sound.stopAll();
	this.music = this.add.audio('factory');
	if (!$.jStorage.get("mute")) this.music.play('', 0, 1, true);
	
	var debugLevel = false;
	if (Consts.DEBUG.GOTO_FACTORY) {
		debugLevel = true;
	}
	else if (!GameState.currentFactory.resourceName) {
		console.error("Unknown resource name, using debug level");
		debugLevel = true;
	}
	if (debugLevel) {
		GameState.currentFactory = {
			resourceName: "apple",
			regionName: "Italia",
			buildings: [], // [[buildingName]]
			additionalWorkers: [], // [{i,j}]
			benefits: 0
		};
	}

	// Level state
	state.data = {
		decorBuildings: {}, // {name:building
		stockBuildings: {},
		liveBuildings: [],
		tileGrid: null,
		heldBuilding: null,
		preview: null,
		newMoney: GameState.money,
		tiles: [], // write with setTile only
		filters: [],
		texts: {
			money: null,
			benefits: null,
			date: null
		},
	};
	
	state.currentLevel = {};
	state.currentLevel.resource = GameDB.resources[GameState.currentFactory.resourceName];
	state.currentLevel.field = GameDB.fields[state.currentLevel.resource.field];
	state.currentLevel.availableBuildingNames = computeAvailableBuildings(GameState.currentFactory.resourceName);
	state.currentLevel.factoryRunner = FactoryRunner.createFactoryRunner(state);
	
	state.shownErrors = {};
	
}

state.create = function() {
	
	// General GUI

	var currentField = state.currentLevel.field;
	var background = this.add.image(0, 0, currentField.background);
	if (currentField.background.indexOf('coasts') != -1) {
		var waterFilter = this.add.filter('Water');
		background.filters = [waterFilter];
		state.data.filters.push(waterFilter);
	}
	var guiBar = this.add.sprite(0, this.world.height - 100, 'gui-bottom');
	var guiBar2 = this.add.sprite(0, 0, 'gui-top');
	
	state.data.tileGrid = state.add.tileSprite(currentField.grid.x, currentField.grid.y,
		Consts.TILE_SIZE * currentField.grid.cols + 1, Consts.TILE_SIZE * currentField.grid.lines + 1,
		'tile');
	state.data.tileGrid.cols = currentField.grid.cols;
	state.data.tileGrid.lines = currentField.grid.lines;
	state.physics.enable(state.data.tileGrid);
	
	state.data.preview = FactoryPreview.createPreview(state, state.data.tileGrid, currentField.zones);
	
	var moneyLabel = this.add.text(guiBar2.x + 50, guiBar2.y + 15, 'Money',
		{font: 'bold 10pt Arial', fill: 'white'});
	moneyLabel.anchor.set(.5,.5);
	state.data.texts.money = this.add.text(guiBar2.x  + 50, guiBar2.y + 55, '',
		{font: 'bold 16pt Arial', fill: 'white', align: 'center'});
	state.data.texts.money.anchor.set(.5,.5);
	state.data.texts.money.refreshText = function() {
		state.data.texts.money.setText(state.data.newMoney + '\ncoins'); // TODO Draw coins
	};
	state.data.texts.money.refreshText();
	
	var benefitsLabel = this.add.text(guiBar2.x + 130, guiBar2.y + 15, 'This month',
		{font: 'bold 10pt Arial', fill: 'white', align: 'center'});
	benefitsLabel.anchor.set(.5,.5);
	state.data.texts.benefits = this.add.text(guiBar2.x + 130, guiBar2.y + 55, '',
		{font: 'bold 16pt Arial', fill: '#5F5', align: 'center'});
	state.data.texts.benefits.anchor.set(.5,.5);;
	state.data.texts.benefits.refreshText = function() {
		state.data.texts.benefits.setText(GameState.currentFactory.benefits + '\ncoins'); // TODO Draw coins
	};

	var confirmAndExit = Gui.createButton(this, state.world.centerX - 200, 150, "Confirm & Exit", function() {
		GameState.money = state.data.newMoney;
        GameState.saveFactory(GameState.currentFactory);
		state.state.start('worldmap');
	}, {centered: true, size: 15, sprite: 'gui-long-active'});
	confirmAndExit.visible = false;
	
	var doStop = function() {
		state.currentLevel.factoryRunner.stop();
		GameState.currentFactory.benefits = computeInitialBenefits();
		state.data.texts.benefits.refreshText();
		play.visible = true;
		stop.visible = false;
		state.data.trash.visible = true;
		state.data.texts.date.visible = false;
		_.each(state.data.liveBuildings, function(building) {
			if (building.gui) {
				building.gui.show();
			}
		});
	}
	var tryAgain = Gui.createButton(this, state.world.centerX + 200, 150, "Try again", doStop,
		{centered: true, size: 15, sprite: 'gui-long-active'});
	tryAgain.visible = false;
	
	state.data.texts.date = this.add.text(state.world.centerX, 35, '',
		{font: 'bold 18pt Arial', fill: 'black', align: 'center'});
	state.data.texts.date.anchor.set(.5,.5);
	state.data.texts.date.visible = false;
	state.data.texts.date.refreshText = function() {
		var currentDay = state.currentLevel.factoryRunner.currentDay || 1;
		state.data.texts.date.setText(currentDay == 31 ? 'End of\nmonth' : 'Day ' + currentDay); // TODO use moment
		if (currentDay == 31) {
			confirmAndExit.visible = true;
			tryAgain.visible = true;
            state.world.bringToTop(tryAgain);
			confirmAndExit.label.setText("Confirm and exit\nBenefits: "
				+ GameState.currentFactory.benefits + "c per month");
            state.world.bringToTop(confirmAndExit);
		}
		else {
			confirmAndExit.visible = false;
			tryAgain.visible = false;
		}
	};
	state.data.texts.date.refreshText();
	
	var play = this.add.button(guiBar2.x + 230, guiBar2.y + 25, 'play', function() {
		state.currentLevel.factoryRunner.start(20);
		play.visible = false;
		stop.visible = true;
		state.data.trash.visible = false;
		state.data.texts.date.visible = true;
	});
	play.input.useHandCursor = true;
	play.anchor.set(.5,0);
	
	var stop = this.add.button(guiBar2.x + 230, guiBar2.y + 25, 'stop', doStop);
	stop.visible = false;
	stop.input.useHandCursor = true;
	stop.anchor.set(.5,0);
		
	var forward = this.add.button(guiBar2.x + 300, guiBar2.y + 25, 'forward', function() {
		state.currentLevel.factoryRunner.start(3);
		play.visible = false;
		stop.visible = true;
		state.data.trash.visible = false;
		state.data.texts.date.visible = true;
	});
	forward.input.useHandCursor = true;
	forward.anchor.set(.5,0);
	
	// Building buttons
	
	state.currentLevel.availableBuildingNames.forEach(function(buildingTypeName, i) {
		var buildingType = GameDB.buildingTypes[buildingTypeName];
		var decorLabel = this.add.text(guiBar.x + (Consts.TILE_SIZE + 40) * i + 70,
			guiBar.y + 8, buildingType.label,
			{font: '10pt Arial', fill: 'white'});
		decorLabel.anchor.set(.5, 0);
		var decorBuilding = this.add.sprite(guiBar.x + (Consts.TILE_SIZE + 40) * i + 70,
			guiBar.y + 68, buildingTypeName);
		decorBuilding.anchor.set(.5);
		decorBuilding.inputEnabled = true;
		state.data.decorBuildings[buildingTypeName] = decorBuilding;
		var description = this.add.group();
		description.descriptionBg = this.add.sprite(decorLabel.x - decorLabel.width + 30,
			decorLabel.y - 80, 'gui-long');
		description.descriptionLabel = this.add.text(decorLabel.x - decorLabel.width + 40,
			decorLabel.y - 70, generateDescription(buildingType),
			{font: '10pt Arial', fill: 'white'});
		description.show = function() {
			description.descriptionBg.visible = true;
			description.descriptionLabel.visible = true;
            state.world.bringToTop(description.descriptionBg);
            state.world.bringToTop(description.descriptionLabel);
		};
		description.hide = function() {
			description.descriptionBg.visible = false;
			description.descriptionLabel.visible = false;
		};
		description.hide();
		decorBuilding.description = description;
		
		createStockBuilding(buildingTypeName);
		var decorLabel = this.add.text(guiBar.x + (Consts.TILE_SIZE + 40) * i + 60,
			guiBar.y + 25, buildingType.cost + 'c',
			{font: '8pt Arial', fill: 'yellow'});
	}, this);
	
	// Level restoration
	var stockBuildingsCache = _.clone(state.data.stockBuildings);
	var additionalWorkerCounts = _.countBy(GameState.currentFactory.additionalWorkers, function(tile) {
	  return tile.i * 100 + tile.j;
	});
	GameState.currentFactory.additionalWorkers = []; // will be repopulated by createBuildingGui
	_.each(GameState.currentFactory.buildings, function(col, i) {
		_.each(col, function(buildingType, j) {
			if (buildingType) {
				var building = createStockBuilding(buildingType);
				building.tile = {i:i,j:j};
				building.x = state.data.tileGrid.x + Consts.TILE_SIZE * (i + .5);
				building.y = state.data.tileGrid.y + Consts.TILE_SIZE * (j + .5);
				building.additionalWorkers = additionalWorkerCounts[i * 100 + j];
				setTile(i, j, building);
				createBuildingGui(building);
				state.data.liveBuildings.push(building);
			}
		});
	});
	state.data.stockBuildings = stockBuildingsCache; // HACK Prevent restoration from breaking stock buildings references
	
	// Initial benefits
	
	GameState.currentFactory.benefits = computeInitialBenefits();
	state.data.texts.benefits.refreshText();
	
	// Trash
	
	state.data.trash = this.add.sprite(this.world.centerX, 32, 'trash');
	state.data.trash.anchor.set(.5);
	state.physics.enable(state.data.trash);
	
	// Menu
	
	var popup = state.add.sprite(0, 0, 'gui-popup'); // TODO Hide on click
	popup.visible = false;
	
	var worldMapButton = Gui.createButton(state, state.world.width/2, state.world.height/2 - 100, 'Cancel to world map', function() {
		state.state.start('worldmap');
	}, {centered: true});
	worldMapButton.visible = false;
	
	var tutorialButton = Gui.createButton(state, state.world.width/2, state.world.height/2, 'Building tutorial', function() {
		worldMapButton.visible = !worldMapButton.visible;
		tutorialButton.visible = !tutorialButton.visible;
		resumeButton.visible = !resumeButton.visible;
		popup.visible = !popup.visible;
		showTutorial();
	}, {centered: true});
	tutorialButton.visible = false;
	
	var resumeButton = Gui.createButton(state, state.world.width/2, state.world.height/2 + 100, 'Resume', function() {
		worldMapButton.visible = !worldMapButton.visible;
		tutorialButton.visible = !tutorialButton.visible;
		resumeButton.visible = !resumeButton.visible;
		popup.visible = !popup.visible;
	}, {centered: true});
	resumeButton.visible = false;
	
	var menuIcon = state.add.button(state.world.width - 64, 15, 'menu_icon', function() {
		worldMapButton.visible = !worldMapButton.visible;
		tutorialButton.visible = !tutorialButton.visible;
		resumeButton.visible = !resumeButton.visible;
		popup.visible = !popup.visible;
        if(popup.visible){
            state.world.bringToTop(popup);
            state.world.bringToTop(worldMapButton);
            state.world.bringToTop(tutorialButton);
            state.world.bringToTop(resumeButton);
        }
	}, state, 1, 0, 2, 1);
	
	// Tutorial
	
	if (!GameState.tutorialShown) {
		GameState.tutorialShown = 1;
		showTutorial();
	}
	
	// Mute
	
	Gui.createMuteButton(state, 'factory', true);
	
};

state.update = function() {

	// Update runner
	state.currentLevel.factoryRunner.update();
	
	var draggableBuildings = _.without(_.union(_.values(state.data.stockBuildings), state.data.liveBuildings), null);
	if (state.currentLevel.factoryRunner.isRunning()) {
		// Don't drag during run
		if (draggableBuildings[0].input.draggable) {
			_.each(draggableBuildings, function (building) {	
				try {
                    if(building.input && building.input.isDragged){
                        building.input.disableDrag();
                        building.input.bringToTop = false;
                    }
				}
                catch(err) {
                    console.error("[catch] Error disable draggable object: "+err);
                }
			});
		}
	}
	else {
		// Restore drag when stopped
        _.each(draggableBuildings, function (building) {	
            if (!building.input.draggable) {
                building.input.enableDrag(true, true);
            }
        });

		// Toggle snap to grid
		_.each(draggableBuildings, function (building) {
			if (building.input.isDragged) {
				var buildingType = GameDB.buildingTypes[building.type];
				state.data.heldBuilding = building;
				state.data.preview.enablePreview(buildingType);
				if (state.data.tileGrid.body.hitTest(state.input.x, state.input.y)) {
					if (!building.input.snapOnDrag) {
						building.input.enableSnap(Consts.TILE_SIZE, Consts.TILE_SIZE,
							true, true,
							state.data.tileGrid.x + Consts.TILE_SIZE/2,
							state.data.tileGrid.y + Consts.TILE_SIZE/2);
					}
					state.data.preview.enableRange(
						{i: Math.floor((state.input.x - state.data.tileGrid.x) / Consts.TILE_SIZE),
						 j: Math.floor((state.input.y - state.data.tileGrid.y) / Consts.TILE_SIZE)},
						buildingType);
				}
				else {
					state.data.preview.disableRange();
					if (building.input.snapOnDrag) {
						building.input.disableSnap();
					}
				}
			}
		});

	}
	
	// Filters
	_.each(state.data.filters, function(filter) {
		filter.update();
	});
	
};

return state;

function createStockBuilding(type) {
	var matchingDecor = state.data.decorBuildings[type];
	
	var building = state.add.sprite(matchingDecor.x, matchingDecor.y, type);
	building.anchor.set(.5);
		
	building.tile = null; // {i,j}
	building.type = type;
	
	state.physics.enable(building);
	building.inputEnabled = true;
	building.input.enableDrag(true, true);
	building.input.useHandCursor = true;
	building.events.onInputOver.add(function() {
		if (!building.tile) {
			matchingDecor.description.show();
		}
	});
	building.events.onInputOut.add(function() {
		if (!building.tile) {
			matchingDecor.description.hide();
		}
	});
	building.events.onInputDown.add(function() {
		matchingDecor.description.hide();
		if (building.gui) {
			building.gui.destroy();
		}
	});
	building.events.onInputUp.add(function() {
		state.data.heldBuilding = null;
		state.data.preview.disablePreview();
		state.data.preview.disableRange();
		
		var failedAssignment = true;
		var previousTile = building.tile;
		if (state.physics.arcade.intersects(state.data.tileGrid, building)) {
			var tile = {
				i: Math.floor((building.x - state.data.tileGrid.x) / Consts.TILE_SIZE),
				j: Math.floor((building.y - state.data.tileGrid.y) / Consts.TILE_SIZE)
			};
			var buildingType = GameDB.buildingTypes[building.type];
			// Move success
			if (tile.i >= 0 && tile.i < state.data.tileGrid.cols // in x bounds
					&& tile.j >= 0 && tile.j < state.data.tileGrid.lines // in y bounds
					&& state.data.preview.isAllowed(buildingType, tile.i, tile.j) // allowed target tile
					&& !getTile(tile.i, tile.j)) { // not already taken
				if (buildingType.cost <= state.data.newMoney || previousTile) { // have enough money
					if (previousTile) {
						// Moved building
						setTile(previousTile.i, previousTile.j, null);
					}
					else {
						// Created building 
						spendMoney(buildingType.cost);
						state.data.liveBuildings.push(building);
						createStockBuilding(type);
					}
                    setTile(tile.i, tile.j, building);
					createBuildingGui(building);
					
					GameState.currentFactory.benefits = computeInitialBenefits();
					state.data.texts.benefits.refreshText();
					failedAssignment = false;
				}
				else {
					Gui.notifyForbidden(state, state.data.texts.money.x, state.data.texts.money.y + 50);
				}
			}
		}
		if (failedAssignment) {
			var target, callback, buildGui = false;
			
			// Failed to create
			if (!previousTile) {
				target = state.data.decorBuildings[building.type];
			}
			
			// Move to trash
			else if (state.data.trash.body.hitTest(state.input.x, state.input.y)) {
				target = null;
				state.data.liveBuildings = _.without(state.data.liveBuildings, building);
				setTile(previousTile.i, previousTile.j, null);
				var destroyBuilding = state.add.tween(building);
				destroyBuilding.to({ width: 0, height: 0 }, 400, Phaser.Easing.Quadratic.InOut, true);
				destroyBuilding.onComplete.add(function() {
					building.destroy();
					spendMoney(-buildingType.cost);
					GameState.currentFactory.benefits = computeInitialBenefits();
					state.data.texts.benefits.refreshText();
				});
			}
			
			// Failed to move
			else {
				buildGui = true;
				target = {
					x: state.data.tileGrid.x + (previousTile.i + .5) * Consts.TILE_SIZE,
					y: state.data.tileGrid.y + (previousTile.j + .5) * Consts.TILE_SIZE
				};
			}
			if (target) {
				var resetBuildingPos = state.add.tween(building);
				resetBuildingPos.to({ x: target.x, y: target.y }, 400, Phaser.Easing.Quadratic.InOut, true);
				if (buildGui) {
					resetBuildingPos.onComplete.add(function() {
						createBuildingGui(building);
					});
				}
			}
		}
	});
	
	state.data.stockBuildings[type] = building;
	
	return building;
}

function createBuildingGui(building) {
	if (building.gui) {
		building.gui.destroy();
	}
	if (GameDB.buildingTypes[building.type].category == "primary") {
		return; // No workers
	}
	
	var left = building.x - building.width/2;
	var top = building.y + 15;
	
	building.additionalWorkers = building.additionalWorkers || 0;
	building.gui = {};
	building.gui.tile = building.tile;
	building.gui.smallWorker = state.add.sprite(left, top, 'worker');
	building.gui.smallWorker.scale.set(.35);
	building.gui.workerCounter = state.add.text(left + 15, top - 3, building.additionalWorkers+1,
		{font: 'bold 11pt "Arial Black"', fill: 'white'});
	building.gui.workerCounter.setShadow(1, 1, 'rgba(0, 0, 0, 1)', 0);
	building.gui.minus = state.add.button(left + 30, top, 'minus', function() {
		if (building.additionalWorkers > 0) {
			removeAdditionalWorker(building.tile);
			building.additionalWorkers--;
			if (building.additionalWorkers == 0) {
				building.gui.minus.alpha = .5;
			}
			building.gui.workerCounter.setText(building.additionalWorkers+1);
		}
		building.gui.plus.alpha = 1;
	});
	building.gui.minus.input.useHandCursor = true;
	if (building.additionalWorkers == 0) {
		building.gui.minus.alpha = .5;
	}
	var MAX_WORKERS = 5;
	building.gui.plus = state.add.button(left + 45, top, 'plus', function() {
		if (building.additionalWorkers < MAX_WORKERS-1) {
			addAdditionalWorker(building.tile);
			building.additionalWorkers++;
			if (building.additionalWorkers == MAX_WORKERS-1) {
				building.gui.plus.alpha = .5;
			}
			building.gui.workerCounter.setText(building.additionalWorkers+1);
		}
		building.gui.minus.alpha = 1;
	});
	building.gui.plus.input.useHandCursor = true;
	if (building.additionalWorkers == MAX_WORKERS-1) {
		building.gui.plus.alpha = .5;
	}
	building.gui.show = function() {
		building.gui.smallWorker.visible = true;
		building.gui.workerCounter.visible = true;
		building.gui.minus.visible = true;
		building.gui.plus.visible = true;
	}
	building.gui.hide = function() {
		building.gui.smallWorker.visible = false;
		building.gui.workerCounter.visible = false;
		building.gui.minus.visible = false;
		building.gui.plus.visible = false;
	}
	building.gui.destroy = function() {
		removeAllAdditionalWorkers( building.tile || building.gui.tile);
		building.gui.smallWorker.destroy();
		building.gui.workerCounter.destroy();
		building.gui.minus.destroy();
		building.gui.plus.destroy();
	}
	for (var i = 0; i < building.additionalWorkers; i++) {
		addAdditionalWorker(building.tile);
	}
}

function addAdditionalWorker(tile) {
	GameState.currentFactory.additionalWorkers.push(tile);
	GameState.currentFactory.benefits = computeInitialBenefits();
	state.data.texts.benefits.refreshText();
}

function removeAdditionalWorker(tile) {
	var alreadyRemoved = false;
	GameState.currentFactory.additionalWorkers = _.reject(
		GameState.currentFactory.additionalWorkers, function(additionalWorker) {
			if (!alreadyRemoved && additionalWorker.i == tile.i && additionalWorker.j == tile.j) {
				alreadyRemoved = true;
				return true;
			};
			return false;
		});
	GameState.currentFactory.benefits = computeInitialBenefits();
	state.data.texts.benefits.refreshText();
}

function removeAllAdditionalWorkers(tile) {
	GameState.currentFactory.additionalWorkers = _.reject(
		GameState.currentFactory.additionalWorkers, function(additionalWorker) {
			return additionalWorker.i == tile.i && additionalWorker.j == tile.j;
		});
	GameState.currentFactory.benefits = computeInitialBenefits();
	state.data.texts.benefits.refreshText();
};

function computeInitialBenefits() {
	var workers = GameState.currentFactory.additionalWorkers.length;
	GameState.currentFactory.buildings.forEach(function(col) {
		if (col) {
			col.forEach(function(tile) {
				if (tile && GameDB.buildingTypes[tile].category != "primary") {
					workers++;
				};
			});
		}
	});
	return -GameDB.SALARY * workers;
}

function getTile(i, j) {
	return (state.data.tiles[i] !== undefined) ? state.data.tiles[i][j] : null;
}

function setTile(i, j, building) {
	if (!state.data.tiles[i]) {
		state.data.tiles[i] = [];
	}
	if (!GameState.currentFactory.buildings[i]) {
		GameState.currentFactory.buildings[i] = [];
	}
	state.data.tiles[i][j] = building;
	GameState.currentFactory.buildings[i][j] = building ? building.type : null;
	if (building) {
		building.tile = {i: i, j: j};
	}
}

function computeAvailableBuildings(resourceName) {
	var buildingName = GameDB.resources[resourceName].produceBuilding;
	if (!buildingName) {
		console.error("Building name not found for resource " + resourceName);
		return ["exportTruck"];
	}
	
	var currentBuilding = GameDB.buildingTypes[buildingName];
	if (!currentBuilding) {
		console.error("Building not found for resource " + resourceName);
		return ["exportTruck"];
	}
	
	if (resourceName == "potatosalad") {
		return ["potatosalad"];
	}
	else { 
		var buildings = [/*"importTruck",*/ buildingName];
		if (currentBuilding.unlock !== undefined) {
			buildings = _.union(buildings, currentBuilding.unlock);
			buildings = _.union(buildings, computeUnlocked(currentBuilding));
		}
		buildings.push("exportTruck");
		return buildings;
	}
}

function computeUnlocked(currentBuilding) {
	var unlocked = [];
	_.each(currentBuilding.unlock, function (buildingName) {
		var building = GameDB.buildingTypes[buildingName];
		if (building.unlock !== undefined) {
			unlocked = _.union(unlocked, building.unlock);
			unlocked = _.union(unlocked, computeUnlocked(building));
		}
	});
	return unlocked;
};

function spendMoney(amount) {
	state.data.newMoney -= amount;
	Gui.notifySpentMoney(state, 
		state.data.texts.money.x, state.data.texts.money.y + 40,
		//building.x, building.y - 40,
		-amount);
	state.data.texts.money.refreshText();
}

function generateDescription(buildingType) {
	var description = buildingType.description ?  buildingType.description : '(No description)';
	description += "\n";
	/*if (buildingType.category == "primary") {
		description += "No worker needed. ";
	}*/
	if (buildingType.unlock) {
		description += "Unlocks: ";
		_.each(buildingType.unlock, function(building, i) {
			description += GameDB.buildingTypes[building].label;
			description += (i < buildingType.unlock.length - 1) ? ", " : ". ";
			if (i == 1 && buildingType.unlock.length > 2) description += "\n"; 
		});
	}
	if (buildingType.constraints) {
		var constraintCount = _.keys(buildingType.constraints).length;
		if (constraintCount > 0) {
			description += "Constraints: ";
			var i = 0;
			_.each(buildingType.constraints, function(value, name) {
				if (name == 'col' && value == -1) description += 'Only on last column';
				if (name == 'col' && value == 0) description += 'Only on first column';
				if (name == 'col' && value > 0) description += 'Only on column ' + value;
				if (name == 'zone') description += 'Only on ' + value + ' tiles';
				if (name == 'range') description += 'Input distance < ' + (value+1);
				if (name == 'rangeX') description += 'Input column dist. <  ' + (value+1);
				if (name == 'rangeY') description += 'Input row dist. <  ' + (value+1);
				description += (i++ < constraintCount - 1) ? ", " : ". ";
			});
		}
	}
	return description;
}

function showTutorial() {
	if (Consts.DEBUG.DISABLE_TUTORIAL) return;

	var popupGroup = state.add.group();
	var popup = state.add.sprite(state.world.centerX, state.world.centerY, 'gui-popup');
	popup.anchor.set(.5);
	popup.inputEnabled = true;
	popup.input.useHandCursor = true;
	popup.events.onInputDown.add(function() {
		var fadeOut = state.add.tween(popupGroup);
		fadeOut.to({alpha: 0., y: 0, x: 950}, 1000, Phaser.Easing.Quadratic.InOut, true);
		var fadeOutScale = state.add.tween(popupGroup.scale);
		fadeOutScale.to({x: 0., y: 0.}, 1000, Phaser.Easing.Quadratic.InOut, true);
		fadeOutScale.onComplete.add(function() {
			popupGroup.destroy();
		});
	});
	popupGroup.add(popup);
	
	var introText = state.add.text(280, 110,
		  'from: your.mom@corponnected.world\n'
		+ 'topic: How to build a factory\n\n'
		+ 'Dear collaborator,\n\n'
		+ 'Building factories is very easy:\n'
		+ '1. Add some buildings. They all depend\n   on each other, so read the docs.\n'
		+ '2. Press Play to see how things turn out.\n'
		+ '3. If you\'re satisfied with your profits,\n   confirm your factory at the end\n   of the month.\n\n'
		+ 'Also:\n'
		+ '- If you don\'t like a building,\ndrop it in the trash (neat!)\n'
		+ '- If your worker is too slow, add some.\n   not that these suckers cost profits.\n\n'
		+ 'And don\'t forget: Everything is connected!\n\n'
		+ 'Regards,\nMom',
		{font: '11pt Courier', fill: 'white'});
	popupGroup.add(introText);
	
	popupGroup.alpha = 0.
	var fadeIn = state.add.tween(popupGroup);
	fadeIn.to({alpha: 1., y: -30}, 1000, Phaser.Easing.Quadratic.InOut, true);

}

});
