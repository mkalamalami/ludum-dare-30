define(['game-state', 'game-db', 'gui', 'consts'], function(GameState, GameDB, Gui, Consts) {
    var GUI_TOP_MARGIN_Y = 20;

    var textStyles = {
        factory: {font: "Bold 12px Arial", fill: "#000000", align: "center"},
        money: {font: "Bold 16px Arial", fill: "#eab512", align: "center"},
        gameinfos: {font: "Bold 16px Arial", fill: "black", align: "center"},
        regionName: {font: "20px Arial", fill: "#FFFFFF", align: "center"},
        resourceField: {font: "16px Arial", fill: "#FFFFFF", align: "center"},
        benefitsField: {font: "18px Arial", fill: "#4CFF00", align: "center"},
        enoughMoney: {font: "18px Arial", fill: "#4CFF00", align: "center"},
        notEnoughMoney: {font: "18px Arial", fill: "#FF0000", align: "center"},
        dataTextField: {font: "10px Arial", fill: "#A0A0A0", align: "center"}
    };

    WorldMapUI = function(state, worldMap) {
        this.state = state;
        this.worldMap = worldMap;
    }

    WorldMapUI.prototype.create = function() {
	
        var self = this;
        // Create worldmap
        this.waterFilter = this.state.add.filter('Water');
        this.map = this.state.add.sprite(0, this.state.world.height / 2, 'world_map');
        this.map.anchor = new Phaser.Point(0, 0.5);
        this.map.filters = [this.waterFilter];

        // Create interfaces
		
        this.guiTop = this.state.add.sprite(0, 0, 'gui-top');
		
        this.logo = this.state.add.sprite(10, this.state.world.height - 50, 'logo');
        this.logo.scale.setTo(0.25, 0.25);
        
        this.money_sprite = this.state.add.sprite(30, GUI_TOP_MARGIN_Y + 10, 'coin_stack');
        this.money = this.state.add.text(30 + 16 + 5, GUI_TOP_MARGIN_Y - 5, "", textStyles.money);
        this.money_sprite.scale.setTo(0.5, 0.5);

        this.benefits = this.state.add.text(30 + 16 + 5, GUI_TOP_MARGIN_Y + 15, "", textStyles.money);
		
        this.tax = this.state.add.text(30 + 16 + 5, GUI_TOP_MARGIN_Y + 35, "", textStyles.money);

        this.date = this.state.add.text(this.state.world.centerX, GUI_TOP_MARGIN_Y, "", textStyles.gameinfos);
        this.date.anchor = new Phaser.Point(0.5, 0);

        this.nextTurnIcon = this.state.add.button(230, 28, 'menu_next_turn', this.onNextTurnInputClick, this, 1, 0, 2, 1);
		this.nextTurnIcon.input.useHandCursor = true;
		
        // Create regions
        this.createAllRegions();
        this.update(false);

        this.menuIcon = this.state.add.button(this.state.world.width - 64, 15, 'menu_icon', this.onMenuClick, this, 1, 0, 2, 1);

        // Create Menu
        this.groupMenu = this.state.add.group();
        this.groupMenu.visible = false;
        Gui.createPopup(this.state, this.groupMenu, true);

        var button = Gui.createButton(this.state, this.state.world.centerX, 150, "Save (in " + GameState.gameName + ")", function() {
            GameState.save();
            self.groupMenu.visible = false;
        }, {centered: true});
        this.groupMenu.add(button);

        button = Gui.createButton(this.state, this.state.world.centerX, 250, "Strategy tutorial", function() {
            self.groupMenu.visible = false;
			showTutorial(self.state);
        }, {centered: true});
        this.groupMenu.add(button);
		
        button = Gui.createButton(this.state, this.state.world.centerX, 350, "Resume", function() {
            self.groupMenu.visible = false;
        }, {centered: true});
        this.groupMenu.add(button);

        button = Gui.createButton(this.state, this.state.world.centerX, 450, "Quit", function() {
            this.state.start('mainmenu');
        }, {centered: true});
        this.groupMenu.add(button);
		
		// Show tutorial
		if (GameState.tutorialShown == 1) {
			GameState.tutorialShown = 2;
			showTutorial(this.state);
		}
		
    }

    WorldMapUI.prototype.update = function(notify) {
        this.money.text = "Money: " + GameState.money + " coins";
        this.benefits.text = "Benefits: " + (GameState.getMonthlyBenefits()+Math.floor(GameState.tax)) + " co/mo";
        this.tax.text = "Tax: " + Math.floor(GameState.tax) + " co/mo";
        this.date.text = GameState.date.format('MMMM YYYY');
        if (notify) {
            Gui.notifySpentMoney(this.state, this.money.x + 40, this.money.y + 60, GameState.lastBenefits);
        }
		
		// Show benefits
		if (GameState.showInternReport) {
			GameState.showInternReport = false;
			showInternReport(this.state, this);
		}
		
		// Show game over
		if (GameState.money < 0) {
			showGameOver(this.state);
		}
		
		Gui.createMuteButton(this.state, 'world', true);
    }

    WorldMapUI.prototype.drawFactoryInfo = function(x, y, name, resources) {

        var width = 36 * resources.length + 5;
        var height = 55;

        // If name is larger than available resources
        if (name.length * 8 + 10 > width) {
            width = name.length * 8 + 10;
        }

        var graphics = this.state.add.graphics(0, 0);
        graphics.smoothed = false;
        graphics.beginFill(0xffffff, 0.9);
        graphics.lineStyle(0.8, 0x3333AA, 1);
        graphics.drawRect(x, y, width, height);
        graphics.endFill();


        var text_name = this.state.add.text(x + 4, y + 4, name, textStyles.factory);

        var group = this.state.add.group();
        group.add(graphics);
        group.add(text_name);

        _.each(resources, function(resource, i) {
            var sprite = this.state.add.sprite(x + 4 + i * 36, y + 19, resource.toLowerCase());
           // sprite.scale.setTo(0.5, 0.5);
            group.add(sprite);
        }, this);

        group.visible = false;
        return group;
    }

    WorldMapUI.prototype.createAllRegions = function() {
        this.regions = [];
		
		// Compute owned regions
		var ownOne = false;
        _.each(GameDB.regions, function(region, regionName) {
			_.each(region.resources, function(resourceName) {
				region.owned = region.owned || !!GameState.getFactory(regionName, resourceName);
			});
			ownOne = ownOne || region.owned;
        }, this);
		
		// Compute reachable regions (all if we have none);
		if (ownOne) {
			_.each(GameDB.regions, function(region, regionName) {
				if (region.owned) {
					region.reachable = true;
				}
				else {
					region.reachable = false;
					
					var closestRegion;
					var closestRegionDistance = 999;
					_.each(GameDB.regions, function(candidateRegion, candidateRegionName) {
						var d = this.state.physics.arcade.distanceBetween(region, candidateRegion);
						if (d > 0 && d < closestRegionDistance) {
							closestRegion = candidateRegion;
							closestRegionDistance = d;
						}
					}, this);
					
					var maxDistance = closestRegionDistance * 1.3;
					var forceMutualReach = ["Ohio", "Brazil", "France", "South Africa"];
					var forceMutualReach2 = ["China", "India", "Australia"];
					_.each(GameDB.regions, function(candidateRegion, candidateRegionName) {
						var d = this.state.physics.arcade.distanceBetween(region, candidateRegion);
						if (candidateRegion.owned && d > 0
								&& (d < maxDistance || d < 100)) {
							region.reachable = true;
						}
						
						// Special cases : America VS Europa
						if (!region.reachable
								&& (region.owned || candidateRegion.owned)
								&& forceMutualReach.indexOf(regionName) != -1
								&& forceMutualReach.indexOf(candidateRegionName) != -1) {
							region.reachable = true;
						}
						if (!region.reachable
								&& (region.owned || candidateRegion.owned)
								&& forceMutualReach2.indexOf(regionName) != -1
								&& forceMutualReach2.indexOf(candidateRegionName) != -1) {
							region.reachable = true;
						}
					}, this);
					
				}
			}, this);
		}
		else {
			_.each(GameDB.regions, function(region, regionName) {
				region.reachable = true;
			});
			var chooseFirstRegionGroup = this.state.add.group();
			var chooseFirstRegionBg = this.state.add.sprite(this.state.world.centerX, 530, 'gui-long');
			chooseFirstRegionBg.anchor.set(.5);
			chooseFirstRegionGroup.add(chooseFirstRegionBg);
			var chooseFirstRegion = this.state.add.text(this.state.world.centerX, 530,
				'Choose your first region\nanywhere in the world',
				{font: '16pt Arial', fill: 'white'});
			chooseFirstRegion.align = 'center';
			chooseFirstRegion.anchor.set(.5);
			chooseFirstRegionGroup.add(chooseFirstRegion);
			chooseFirstRegionGroup.alpha = 0.;
			var fadeIn = this.state.add.tween(chooseFirstRegionGroup);
			fadeIn.to({alpha: 1., y: -30}, 1000, Phaser.Easing.Quadratic.InOut, true);
		}
		
        _.each(GameDB.regions, function(region, regionName) {
            this.regions.push(this.addRegion(regionName, region));
        }, this);
    }

    WorldMapUI.prototype.addRegion = function(name, region) {
		
        regionInput = this.state.add.button(region.x, region.y + 38,
			(region.reachable) ? 'factory_icon' : 'factory_icon_far',
			this.onRegionInputClick, this, 1, 0, 2, 1);
        regionInput.inputEnabled = true;
        regionInput.input.useHandCursor = true;
        regionInput.anchor = new Phaser.Point(0.5, 1);
        regionInput.regionName = name;
        regionInput.region = region;

        regionInput.infos = this.drawFactoryInfo(region.x + 20, region.y - 16, name, region.resources);

        regionInput.events.onInputOver.add(this.onRegionInputOver, this);
        regionInput.events.onInputOut.add(this.onRegionInputOut, this);

        return regionInput;
    }

    WorldMapUI.prototype.onRegionInputClick = function(currentRegionBtn) {
        this.onRegionInputOut(currentRegionBtn);
        // Need to crete and open Region Menu
        var popupGroup = this.state.add.group();
        var popup = Gui.createPopup(this.state, popupGroup);

        var group = this.state.add.group();
        popupGroup.add(group);
        var nameText = this.state.add.text(popup.customPosition.width / 2, 10, currentRegionBtn.regionName, textStyles.regionName);
        nameText.anchor.setTo(0.5, 0);
        group.add(nameText);

        _.each(currentRegionBtn.region.resources, function(resourceName, i) {
            var contentGroup = this.state.add.group();

            var resourceSprite = this.state.add.sprite(15, 0, resourceName);
            resourceSprite.anchor.setTo(0, 0.5);
            contentGroup.add(resourceSprite);

            var resource = GameDB.resources[resourceName];
            var label;
            if (resource.field.indexOf("factory") != -1) {
                label = "[manufactured] "+resource.label;
            } else {
                label = "[basic] "+resource.label;
            }
            var resourceField = this.state.add.text(15 + 32 + 15, 0, label, textStyles.resourceField);
            resourceField.anchor.setTo(0, 0.5);
            contentGroup.add(resourceField);

            // get our data if we own the factory
            var ourFactory = GameState.getFactory(currentRegionBtn.regionName, resourceName);
            var dataText;
            var factoryInput;
            if (ourFactory != null) {
                var benefitsField = this.state.add.text(popup.customPosition.width - 75, 0, ourFactory.benefits + " coins/mo.", textStyles.benefitsField);
                benefitsField.anchor.setTo(1, 0.5);
                contentGroup.add(benefitsField);
                dataText = "(Click to enter in your factory)";
                factoryInput = this.state.add.button(popup.customPosition.width / 2, 50 + i * (69 + 10), "gui-long", this.onOurFactoryInputClick, this);
                factoryInput.input.useHandCursor = true;
                factoryInput.factory = ourFactory;
            } else {
                var cost = GameDB.fields[resource.field].cost;
                var style;

                if (cost > GameState.money) {
                    style = textStyles.notEnoughMoney;
                    dataText = "(You haven't enough money to buy this factory)";
                    factoryInput = this.state.add.button(popup.customPosition.width / 2, 50 + i * (69 + 10), "gui-long-inactive");
                } else if (!currentRegionBtn.region.reachable) {
                    dataText = "(This factory is out of your sphere of influence)";
                    style = textStyles.notEnoughMoney;
                    factoryInput = this.state.add.button(popup.customPosition.width / 2, 50 + i * (69 + 10), "gui-long-inactive");
                }  else {
                    style = textStyles.enoughMoney;
                    dataText = "(Click to buy this factory)";
                    factoryInput = this.state.add.button(popup.customPosition.width / 2, 50 + i * (69 + 10), "gui-long-active", this.onNewFactoryInputClick, this);
                    factoryInput.input.useHandCursor = true;
                    factoryInput.factory = {};
                    factoryInput.factory.name = resourceName;
                    factoryInput.factory.regionName = currentRegionBtn.regionName;
                    factoryInput.factory.cost = cost;
                }
                var costField = this.state.add.text(popup.customPosition.width - 75, 0, cost + " coins", style);
                costField.anchor.setTo(1, 0.5);
                contentGroup.add(costField);
            }
            var dataTextField = this.state.add.text(15 + 32 + 15, 15, dataText, textStyles.dataTextField);
            dataTextField.anchor.setTo(0, 0.5);
            contentGroup.add(dataTextField);

            factoryInput.anchor.setTo(0.5, 0);
            contentGroup.x = 30;
            contentGroup.y = 50 + i * (69 + 10) + (69 / 2);
            contentGroup.z = 2;
            group.add(factoryInput);
            group.add(contentGroup);

        }, this);
        group.x = popup.customPosition.x;
        group.y = popup.customPosition.y;
    }

    WorldMapUI.prototype.onRegionInputOver = function(button) {
        button.infos.visible = true;
        this.state.world.bringToTop(button);
        this.state.world.bringToTop(button.infos);
    }

    WorldMapUI.prototype.onRegionInputOut = function(button) {
        button.infos.visible = false;
    }

    WorldMapUI.prototype.onNewFactoryInputClick = function(button) {
        this.worldMap.buyNewFactoy(button.factory);
    }

    WorldMapUI.prototype.onOurFactoryInputClick = function(button) {
        this.worldMap.enterInFactory(button.factory);
    }

    WorldMapUI.prototype.onMenuClick = function(button) {
        if (this.groupMenu.visible) {
            this.groupMenu.visible = false;
        } else {
            this.groupMenu.visible = true;
            this.state.world.bringToTop(this.groupMenu);
        }
    }

    WorldMapUI.prototype.onNextTurnInputClick = function(button) {
        this.worldMap.nextTurn();
        this.update(true);
    }
	
	function showGameOver(state) {
		var popupGroup = state.add.group();
		var popup = state.add.sprite(state.world.centerX, state.world.centerY, 'gui-popup');
		popup.anchor.set(.5);
		popup.inputEnabled = true;
		popup.input.useHandCursor = true;
		popup.events.onInputDown.add(function() {
			var fadeOut = state.add.tween(popupGroup);
			fadeOut.to({alpha: 0., y: 0}, 1000, Phaser.Easing.Quadratic.InOut, true);
			fadeOut.onComplete.add(function() {
				popupGroup.destroy();
				state.state.start('mainmenu');
			});
		});
		popupGroup.add(popup);
		
		var monthlyBenefits = GameState.getMonthlyBenefits();
		var tax = Math.floor(GameState.tax);
		var factoryBenefits = monthlyBenefits + tax;
		var firedText = state.add.text(280, 110,
			  'from: hr@corponnected.world\n'
			+ 'topic: Bankrupt!\n\n'
			+ 'Dear collaborator,\n\n'
			+ 'We have no more money because of you.\n'
			+ 'You\'re fired!\n\n'
			+ 'With love,\nThe HR',
			{font: '11pt Courier', fill: 'white'});
		popupGroup.add(firedText);
		
		popupGroup.alpha = 0.
		var fadeIn = state.add.tween(popupGroup);
		fadeIn.to({alpha: 1., y: -30}, 1000, Phaser.Easing.Quadratic.InOut, true);

	};
	
	function showInternReport(state, ui) {
		var factoryCount = GameState.findAllPlayerFactories().length;
		if (Consts.DEBUG.DISABLE_TUTORIAL || factoryCount == 0) return;

		var popupGroup = state.add.group();
		var popup = state.add.sprite(state.world.centerX, state.world.centerY, 'gui-popup');
		popup.anchor.set(.5);
		popup.inputEnabled = true;
		popup.input.useHandCursor = true;
		popup.events.onInputDown.add(function() {
			var fadeOut = state.add.tween(popupGroup);
			fadeOut.to({alpha: 0., y: 0}, 1000, Phaser.Easing.Quadratic.InOut, true);
			fadeOut.onComplete.add(function() {
				popupGroup.destroy();
			});
		});
		popupGroup.add(popup);
		
		var monthlyBenefits = GameState.getMonthlyBenefits();
		var tax = Math.floor(GameState.tax);
		var factoryBenefits = monthlyBenefits + tax;
		var benefitsText = state.add.text(280, 110,
			  'from: the.intern@corponnected.world\n'
			+ 'topic: [' + ui.date.text + '] Your monthly report\n\n'
			+ 'Dear collaborator,\n\n'
			+ 'From the past month, you made:\n'
			+ (factoryBenefits>0?'+'+factoryBenefits: factoryBenefits) + ' coins from your '
				+ factoryCount + ((factoryCount==1)?' factory':' factories') + '\n'
			+ '-' + tax + ' coins from tax\n\n'
			+ 'Total: ' + (monthlyBenefits>0?'+'+monthlyBenefits: monthlyBenefits) + ' coins\n'
			+ 'Current money: ' + GameState.money + ' coins\n\n'
			+ 'Regards,\nThe Intern',
			{font: '11pt Courier', fill: 'white'});
		popupGroup.add(benefitsText);
		
		popupGroup.alpha = 0.
		var fadeIn = state.add.tween(popupGroup);
		fadeIn.to({alpha: 1., y: -30}, 1000, Phaser.Easing.Quadratic.InOut, true);

	}

	
	function showTutorial(state) {
		if (Consts.DEBUG.DISABLE_TUTORIAL) return;

		var popupGroup = state.add.group();
		var popup = state.add.sprite(state.world.centerX, state.world.centerY, 'gui-popup');
		popup.anchor.set(.5);
		popup.inputEnabled = true;
		popup.input.useHandCursor = true;
		popup.events.onInputDown.add(function() {
			var fadeOut = state.add.tween(popupGroup);
			fadeOut.to({alpha: 0., y: 0, x: 950}, 1000, Phaser.Easing.Quadratic.InOut, true);
			var fadeOutScale = state.add.tween(popupGroup.scale);
			fadeOutScale.to({x: 0., y: 0.}, 1000, Phaser.Easing.Quadratic.InOut, true);
			fadeOutScale.onComplete.add(function() {
				popupGroup.destroy();
			});
		});
		popupGroup.add(popup);
		
		var introText = state.add.text(280, 110,
			  'from: the.janitor@corponnected.world\n'
			+ 'topic: How to conquer the world\n\n'
			+ 'Dear collaborator,\n\n'
			+ 'Each month, you have the choice between:\n'
			+ '1. Building a new factory\n'
			+ '2. Investing again in an existing factory\n'
			+ '3. Do nothing, to earn some money (but be\n   careful with that, taxes increase\n   slowly with inflation!)\n\n'
			+ 'Hopefully you\'ll soon be able to send a\n'
			+ 'rocket to space and connect us with\nnew worlds!! And thus win the game,\nin case you didn\'t get it yet.\n\n'
			+ 'Tip: You can save your progress anytime.\n'
			+ 'Good luck,\nThe Janitor',
			{font: '11pt Courier', fill: 'white'});
		popupGroup.add(introText);
		
		popupGroup.alpha = 0.
		var fadeIn = state.add.tween(popupGroup);
		fadeIn.to({alpha: 1., y: -30}, 1000, Phaser.Easing.Quadratic.InOut, true);

	}

});
