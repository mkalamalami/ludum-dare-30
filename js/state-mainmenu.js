define(['gui', 'consts', 'game-state'], function(Gui, Consts, GameState) {

	var state = new Phaser.State();

	var waterFilter;
	
	state.preload = function() {
		
		if (!GameState.firstWorldShow) {
			state.sound.stopAll();
			this.music = this.add.audio('title');
			if (!$.jStorage.get("mute")) this.music.play();
		}
		
		waterFilter = this.add.filter('Water');
		var background = this.add.image(0, 0, 'world_map');
		background.filters = [waterFilter];
		
		var popup = this.add.sprite(this.world.centerX, this.world.centerY - 120, 'gui-popup');
		popup.anchor.set(.5);
		
		var introText = this.add.text(280, 175,
			  'from: the.boss@corponnected.world\n'
			+ 'topic: Your objectives\n\n'
			+ 'Dear collaborator,\n'
			+ 'Given that I am leaving for vacation,\n'
			+ 'I leave you in charge of our company.\n'
			+ 'During my absence, could you please\n'
			+ 'take some time to conquer the world?\n'
			+ 'It would be nice to have factories everywhere,\n'
			+ 'and, why not, send a *rocket to space*.\n'
			+ 'Thanks,\nThe Boss',
			{font: '11pt Courier', fill: 'white'});
		
		var logo = this.add.image(this.world.centerX, this.world.centerY - 210, 'logo');
		logo.anchor.set(.5);
		var logoTween = this.add.tween(logo);
		logoTween.to({y: logo.y - 5}, 2000, Phaser.Easing.Sinusoidal.InOut, true, 0, Number.MAX_VALUE, true);
		
		Gui.createButton(this, this.world.centerX, this.world.centerY + 160, "New game", function() {
            GameState.init();
            this.state.start('worldmap');
		}, {centered: true});
		
		Gui.createButton(this, this.world.centerX, this.world.centerY + 240, "Load game", function() {
			this.state.start('gameload');
		}, {centered: true});		// FIXME � d�commenter quand on aura un son
		
		//this.playMusicButton = this.add.button(50, this.world.height - 50, 'soundIcon', this.toggleMusic, this);
        //this.tutoCheckbox = this.add.button(90, this.world.height - 50, 'tuto_checkbox', this.toggleTuto, this);
        
        // get saved tuto
        this.tuto = true;
        var lastTuto = $.jStorage.get("tuto");
        if(lastTuto != undefined && lastTuto != this.tuto){
            this.toggleTuto();
            this.render();
        }
        
		Gui.createMuteButton(this, 'title');
		
		// Debug shortcuts
		if (Consts.DEBUG.GOTO_WORLDMAP) {
			this.state.start('worldmap');
		}
		if (Consts.DEBUG.GOTO_FACTORY) {
			this.state.start('factory');
		}
	};
	


	state.update = function() {
		waterFilter.update();
	};

	state.create = function() {
		this.stage.backgroundColor = '#000000';
	};

	/*
    state.toggleTuto = function () {
		if(this.tuto){
			this.tuto = false;
            this.tutoCheckbox.setFrames(1,1,0,1);
		} else {
			this.tuto = true;
            this.tutoCheckbox.setFrames(0,0,1,0);
		}
        $.jStorage.set("tuto", this.tuto);
	}*/

	return state;

});
